//
//  ProductViewController.m
//  Soufu580
//
//  Created by vidy on 14-4-16.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "ProductViewController.h"
#import "UIImage+Resize.h"
#import "defines.h"
#import "FinProduct.h"
#import "FixProductCell.h"
#import "FloatProductCell.h"
#import "FixProductDetailController.h"
#import "FixProductDetail.h"
#import "FloatProductDetailController.h"
#import "FloatProductDetail.h"

NSInteger kTagFix = 100;
NSInteger kTagFloat = 101;

@interface ProductViewController ()<UITableViewDataSource,UITableViewDelegate>{
    UITableView * fixTable;
    UITableView * floatTable;
    
    UIButton * fixBtn;
    UIButton * floatBtn;
    
    NSMutableArray * fixArr;
    NSMutableArray * floatArr;
    
    NSInteger currentTag;
}

-(void)addContentView;
-(void)clickOnBut:(id)btn;

@end

@implementation ProductViewController

- (id)init {
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"产品" image:nil tag:0];
        [[self tabBarItem] setSelectedImage:[[UIImage imageNamed:@"jiezhangButSel"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        [[self tabBarItem] setImage:[[UIImage imageNamed:@"jiezhangBut"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        
        [[self tabBarItem] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                                   [UIFont systemFontOfSize:12.0], NSFontAttributeName,
                                                   nil] forState:UIControlStateNormal];
        
        //默认显示固定类
        currentTag = kTagFix;
        fixArr = [[NSMutableArray alloc] init];
        floatArr = [[NSMutableArray alloc] init];
        
        for (int i = 0;i < 5;i++){
            FinProduct * pdt = [[FinProduct alloc] init];
            pdt.title = @"中科创-融石新源燕府项目集合资金信托计划  查看详情";
            pdt.bottomLine = @"12个月 100万起";
            pdt.asked = @"近期 10000 人咨询过产品";
            
            [fixArr addObject:pdt];
        }
        for (int i = 0;i < 5;i++){
            FinProduct * pdt1 = [[FinProduct alloc] init];
            pdt1.title = @"私募教父邓启翔";
            pdt1.subTitle=@"普洱茶7号";
            pdt1.bottomLine = @"12个月 100万起";
            pdt1.asked = @"近期 10011 人咨询过产品";
            pdt1.contentA=@"300万元起购";
            pdt1.contentB=@"截止日期：2014－5-5";
            pdt1.contentC=@"10000";
            pdt1.contentD=@"人咨询过产品";
            pdt1.contentF=@"买就送：3000元红包";
            
            [floatArr addObject:pdt1];
        }

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addTopViewWithTitle:@"理财产品" leftButtonTitle:nil rightButtonTitle:nil];
    [self addContentView];
}

-(void)addContentView{
    
    //添加按钮
    fixBtn = [[UIButton alloc] initWithFrame:CGRectMake(5.0, TITLE_HEIGHT+10, DEVICE_WIDTH/2-10, 40.0)];
    [fixBtn setBackgroundImage:[UIImage imageNamed:@"btn_shoufeidefault1"] forState:UIControlStateNormal];
    [fixBtn setBackgroundImage:[UIImage imageNamed:@"btn_shoufeiselected2"] forState:UIControlStateSelected];
    [fixBtn setTitle:@"固定收益类" forState:UIControlStateNormal];
    [fixBtn.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
    [fixBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [fixBtn addTarget:self action:@selector(clickOnBut:) forControlEvents:UIControlEventTouchUpInside];
    fixBtn.tag = kTagFix;
    [self.view addSubview:fixBtn];
    
    floatBtn = [[UIButton alloc] initWithFrame:CGRectMake(DEVICE_WIDTH/2+5.0, TITLE_HEIGHT+10, DEVICE_WIDTH/2-10, 40.0)];
    [floatBtn setBackgroundImage:[UIImage imageNamed:@"btn_shoufeidefault1"] forState:UIControlStateNormal];
    [floatBtn setBackgroundImage:[UIImage imageNamed:@"btn_shoufeiselected2"] forState:UIControlStateSelected];
    [floatBtn setTitle:@"浮动收益类" forState:UIControlStateNormal];
    [floatBtn.titleLabel setFont:[UIFont systemFontOfSize:18.0]];
    [floatBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [floatBtn addTarget:self action:@selector(clickOnBut:) forControlEvents:UIControlEventTouchUpInside];
    floatBtn.tag = kTagFloat;
    [self.view addSubview:floatBtn];
    
    
    //添加表格
    fixTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, TITLE_HEIGHT+55, DEVICE_WIDTH, DEVICE_HEIGHT-TITLE_HEIGHT-BOTTOM_HEIGHT-55) style:UITableViewStylePlain];
    fixTable.delegate = self;
    fixTable.dataSource = self;
    fixTable.backgroundColor = [UIColor clearColor];
    fixTable.bounces = NO;
    fixTable.tag = kTagFix;
    [self.view addSubview:fixTable];
    
    
    floatTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, TITLE_HEIGHT+55, DEVICE_WIDTH, DEVICE_HEIGHT-TITLE_HEIGHT-BOTTOM_HEIGHT-55) style:UITableViewStylePlain];
    floatTable.delegate = self;
    floatTable.dataSource = self;
    floatTable.backgroundColor = [UIColor clearColor];
    floatTable.bounces = NO;
    floatTable.tag = kTagFloat;
    [self.view addSubview:floatTable];
    //默认隐藏浮动收益类
    floatTable.hidden = TRUE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)clickOnBut:(id)btn{
    UIButton * button = (UIButton*)btn;
    
    if (button.tag == kTagFix && currentTag != kTagFix) {
        floatTable.hidden = TRUE;
        fixTable.hidden = FALSE;
        currentTag = kTagFix;
        [fixTable reloadData];
    }else if(button.tag == kTagFloat && currentTag != kTagFloat) {
        fixTable.hidden = TRUE;
        floatTable.hidden = FALSE;
        currentTag = kTagFloat;
        [floatTable reloadData];
    }
}


#pragma mark - UITableView的协议处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == kTagFix) {
        return [fixArr count];
    }else if (tableView.tag == kTagFloat) {
        return [floatArr count];
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger height = 0;
    if (tableView.tag == kTagFix) {
        height= kFixProductCellHeight;
    }else if (tableView.tag == kTagFloat) {
        //需修改为FLoat类型的高度
        height = kFloatProductCellHeight;
    }
    
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == kTagFix) {
        //查看固定收益详情
        FixProductDetail * fix = [[FixProductDetail alloc]init];
        FixProductDetailController * detail = [[FixProductDetailController alloc] initWithProDetail:fix];
        [self.navigationController pushViewController:detail animated:TRUE];
    }else if (tableView.tag == kTagFloat) {
        //查看浮动收益详情
        FloatProductDetail * f = [[FloatProductDetail alloc]init];
        FloatProductDetailController * detail = [[FloatProductDetailController alloc] initWithProDetail:f];
        [self.navigationController pushViewController:detail animated:TRUE];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == kTagFix) {
        //固定收益
        FinProduct * pdt = [fixArr objectAtIndex:indexPath.row];
        
        FixProductCell * cell = [tableView dequeueReusableCellWithIdentifier:kFixProductCellIdentifier];
        
        if (cell==nil) {
            cell = [[FixProductCell alloc] initWithFixProduct:pdt];
            cell.userInteractionEnabled = YES;
        }else{
            [cell updateFixProduct:pdt];
        }
        
        return cell;
    }
    else if (tableView.tag == kTagFloat) {
        //浮动收益
        FinProduct * pdt1 = [floatArr objectAtIndex:indexPath.row];
        
        FloatProductCell * cell1 = [tableView dequeueReusableCellWithIdentifier: kFloatProductCellIdentifier];
        
        if (cell1==nil) {
            cell1 = [[FloatProductCell alloc] initWithFloatProduct:pdt1];
            cell1.userInteractionEnabled = YES;
        }else{
            [cell1 updateFloatProduct:pdt1];
        }
        
        return cell1;

    }
    
    return nil;
}

@end
