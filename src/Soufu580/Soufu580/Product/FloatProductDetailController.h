//
//  FloatProductDetailController.h
//  Soufu580
//
//  Created by 邓启翔 on 14-5-4.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "FloatProductDetail.h"


@interface FloatProductDetailController : BaseVC
-(id)initWithProDetail:(FloatProductDetail *) prjDetail;
@end
