//
//  FixProductCell.h
//  Soufu580
//
//  Created by  apple on 14-4-22.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FinProduct.h"

@interface FloatProductCell : UITableViewCell

- (id)initWithFloatProduct:(FinProduct*)fin;
- (id)updateFloatProduct:(FinProduct*)fin;

@end


extern NSString * kFloatProductCellIdentifier;
extern NSInteger kFloatProductCellHeight;
