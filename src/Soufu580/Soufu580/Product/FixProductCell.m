//
//  FixProductCell.m
//  Soufu580
//
//  Created by  apple on 14-4-22.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "FixProductCell.h"
#import "defines.h"
#import "UIImage+Resize.h"

NSString * kFixProductCellIdentifier = @"FixProductCell";
NSInteger kFixProductCellHeight = 120;

@interface FixProductCell () {
    FinProduct * product;
    
    UIImageView * rateView;
    UILabel * bottomLb;
    UILabel * titleLb;
    UIImageView * presentView;
    UILabel * askLb;
}

- (void)flushCellView;

@end


@implementation FixProductCell

- (id)initWithFixProduct:(FinProduct*)fin{
    
    product = fin;
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kFixProductCellIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        
        //add rate image
        rateView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, kFixProductCellHeight-30, kFixProductCellHeight-30)];
        [rateView setImage:[[UIImage imageNamed:@"u32_normal.png"] resizedImageToFitInSize:CGSizeMake(kFixProductCellHeight-30, kFixProductCellHeight-30) scaleIfSmaller:TRUE]];
        [self addSubview:rateView];
        
        bottomLb = [[UILabel alloc] initWithFrame:CGRectMake(5, kFixProductCellHeight-25, kFixProductCellHeight-20, 20)];
        bottomLb.backgroundColor = [UIColor clearColor];
        bottomLb.font = [UIFont boldSystemFontOfSize:14.0];
        bottomLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        [self addSubview:bottomLb];
        
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(kFixProductCellHeight, 5, DEVICE_WIDTH-kFixProductCellHeight-5, 40)];
        titleLb.backgroundColor = [UIColor clearColor];
        titleLb.font = [UIFont boldSystemFontOfSize:14.0];
        titleLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        titleLb.numberOfLines = 0;
        [self addSubview:titleLb];
        
        //add present image
        presentView = [[UIImageView alloc] initWithFrame:CGRectMake(kFixProductCellHeight, 50, 180, 40)];
        [presentView setImage:[[UIImage imageNamed:@"u35_normal.png"] resizedImageToFitInSize:CGSizeMake(200, 20) scaleIfSmaller:TRUE]];
        [self addSubview:presentView];
        
        askLb = [[UILabel alloc] initWithFrame:CGRectMake(kFixProductCellHeight, kFixProductCellHeight-20, 200, 15)];
        askLb.backgroundColor = [UIColor clearColor];
        askLb.font = [UIFont boldSystemFontOfSize:14.0];
        askLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        [self addSubview:askLb];
    }
    
    [self flushCellView];
    
    return self;
}

- (id)updateFixProduct:(FinProduct*)fin{
    product = fin;
    
    [self flushCellView];
    
    return self;
}

- (void)flushCellView{

    
    // set the logo image
    
    titleLb.text = [NSString stringWithFormat:@"%@",product.title];
    
    bottomLb.text = [NSString stringWithFormat:@"%@",product.bottomLine];
    
    askLb.text = [NSString stringWithFormat:@"%@",product.asked];
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
