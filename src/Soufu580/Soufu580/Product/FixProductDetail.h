//
//  FixProductDetai.h
//  Soufu580
//
//  Created by 邓启翔 on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface FixProductDetail : NSObject{
    NSString * _qixian;
    NSString * _didian;
    NSString * _asked;
    NSString * _touziqidian;
    NSString * _fuxifangshi;
    NSString * _fashouriqi;
    NSString * _zixunrenshu;
    NSString * _zixun;
    NSString * _shuoming;
    NSString * _shouyilv;
}

@property (nonatomic,retain) NSString * qixian;
@property (nonatomic,retain) NSString * didian;
@property (nonatomic,retain) NSString * asked;
@property (nonatomic,retain) NSString * touziqidian;
@property (nonatomic,retain) NSString * fuxifangshi;
@property (nonatomic,retain) NSString * fashouriqi;
@property (nonatomic,retain) NSString * zixunrenshu;
@property (nonatomic,retain) NSString * zixun;
@property (nonatomic,retain) NSString * shuoming;
@property (nonatomic,retain) NSString * shouyilv;

@end