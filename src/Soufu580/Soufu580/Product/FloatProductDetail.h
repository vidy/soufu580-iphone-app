//
//  FloatProductDetail.h
//  Soufu580
//
//  Created by 邓启翔 on 14-5-4.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FloatProductDetail : NSObject
{
    NSString * _jingli;
    NSString * _gongsi;
    NSString * _faxing;
    NSString * _touziqidian;
    NSString * _jiezhiriqi;
    NSString * _rengoufei;
    NSString * _guanglifei;
    NSString * _zixunrenshu;
    NSString * _xiangxijieshao;
    NSString * _shouyilv;
}

@property (nonatomic,retain) NSString * jingli;
@property (nonatomic,retain) NSString * gongsi;
@property (nonatomic,retain) NSString * faxing;
@property (nonatomic,retain) NSString * touziqidian;
@property (nonatomic,retain) NSString * jiezhiriqi;
@property (nonatomic,retain) NSString * rengoufei;
@property (nonatomic,retain) NSString * guanglifei;
@property (nonatomic,retain) NSString * zixunrenshu;
@property (nonatomic,retain) NSString * xiangxijieshao;
@property (nonatomic,retain) NSString * shouyilv;

@end
