//
//  FixProductCell.m
//  Soufu580
//
//  Created by  apple on 14-4-22.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "FloatProductCell.h"
#import "defines.h"
#import "UIImage+Resize.h"

NSString * kFloatProductCellIdentifier = @"FloatProductCell";
NSInteger kFloatProductCellHeight = 150;

@interface FloatProductCell () {
    FinProduct * product;
    
    UIImageView * rateView;
    UIButton * bottomLb;
    UILabel * titleLb;
    UILabel * subtitleLb;
    UIImageView * presentView;
    UILabel * askLb;
    UILabel * contentA;
    UILabel * contentB;
    UILabel * contentC;
    UILabel * contentD;
    UILabel * contentF;
}

- (void)flushCellView;

@end


@implementation FloatProductCell

- (id)initWithFloatProduct:(FinProduct*)fin{
    
    product = fin;
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kFloatProductCellIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        
        //title
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 120, 25)];
        titleLb.backgroundColor = [UIColor clearColor];
        titleLb.font = [UIFont boldSystemFontOfSize:16.0];
        titleLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        titleLb.numberOfLines = 0;
        
        [self addSubview:titleLb];
        //subtitle
        subtitleLb = [[UILabel alloc] initWithFrame:CGRectMake(125, 5, 120, 25)];
        subtitleLb.backgroundColor = [UIColor clearColor];
        subtitleLb.font = [UIFont boldSystemFontOfSize:14.0];
        subtitleLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        subtitleLb.numberOfLines = 0;
        
        [self addSubview:subtitleLb];
        //contentA
        contentA = [[UILabel alloc] initWithFrame:CGRectMake(125, 35, 120, 20)];
        contentA.backgroundColor = [UIColor clearColor];
        contentA.font = [UIFont boldSystemFontOfSize:14.0];
        contentA.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        contentA.numberOfLines = 0;
        
        [self addSubview:contentA];
        
        //contentB
        contentB = [[UILabel alloc] initWithFrame:CGRectMake(125, 60, 200, 20)];
        contentB.backgroundColor = [UIColor clearColor];
        contentB.font = [UIFont boldSystemFontOfSize:14.0];
        contentB.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        contentB.numberOfLines = 0;
        
        [self addSubview:contentB];
        
        //contentC
        contentC = [[UILabel alloc] initWithFrame:CGRectMake(125, 85, 50, 20)];
        contentC.backgroundColor = [UIColor clearColor];
        contentC.textColor=[UIColor redColor];
        contentC.font = [UIFont boldSystemFontOfSize:14.0];
        //contentC.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        contentC.numberOfLines = 0;
        
        [self addSubview:contentC];
        
        //contentD
        contentD = [[UILabel alloc] initWithFrame:CGRectMake(180, 85, 140, 20)];
        contentD.backgroundColor = [UIColor clearColor];
        contentD.font = [UIFont boldSystemFontOfSize:14.0];
        contentD.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        contentD.numberOfLines = 0;
        
        [self addSubview:contentD];
        
        //contentF
        contentF = [[UILabel alloc] initWithFrame:CGRectMake(125, 120, 150, 20)];
        contentF.backgroundColor = [UIColor clearColor];
        contentF.font = [UIFont boldSystemFontOfSize:14.0];
        contentF.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        contentF.numberOfLines = 0;
        
        [self addSubview:contentF];


        //图片
        rateView = [[UIImageView alloc] initWithFrame:CGRectMake(5,35, 100, 80)];
        [rateView setImage:[[UIImage imageNamed:@"u32_normal.png"] resizedImageToFitInSize:CGSizeMake(100, kFloatProductCellHeight-30) scaleIfSmaller:TRUE]];
        [self addSubview:rateView];
        //预约按钮
        bottomLb = [[UIButton alloc] initWithFrame:CGRectMake(5, 120, 100, 25)];
        bottomLb.backgroundColor = [UIColor redColor];
        [bottomLb setTitle:@"预约" forState:UIControlStateNormal];
        [self addSubview:bottomLb];
    }
    
    [self flushCellView];
    
    return self;
}

- (id)updateFloatProduct:(FinProduct*)fin{
    product = fin;
    
    [self flushCellView];
    
    return self; 
}

- (void)flushCellView{
    
    
    // set the logo image
    
    titleLb.text = [NSString stringWithFormat:@"%@",product.title];
    subtitleLb.text = [NSString stringWithFormat:@"%@",product.subTitle];
    
//    bottomLb.text = [NSString stringWithFormat:@"%@",product.bottomLine];
    
    askLb.text = [NSString stringWithFormat:@"%@",product.asked];
    contentA.text=[NSString stringWithFormat:@"%@",product.contentA];
    contentB.text=[NSString stringWithFormat:@"%@",product.contentB];
    contentC.text=[NSString stringWithFormat:@"%@",product.contentC];
    contentD.text=[NSString stringWithFormat:@"%@",product.contentD];
    contentF.text=[NSString stringWithFormat:@"%@",product.contentF];
    
} 

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
