//
//  FixProductCell.h
//  Soufu580
//
//  Created by  apple on 14-4-22.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FinProduct.h"

@interface FixProductCell : UITableViewCell

- (id)initWithFixProduct:(FinProduct*)fin;
- (id)updateFixProduct:(FinProduct*)fin;

@end


extern NSString * kFixProductCellIdentifier;
extern NSInteger kFixProductCellHeight;
