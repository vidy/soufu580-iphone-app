//
//  FixProductDetailController.m
//  Soufu580
//
//  Created by 邓启翔 on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "FixProductDetailController.h"
#import "UIImage+Resize.h"
#import "defines.h"
#import "FixProductDetail.h"

@interface FixProductDetailController ()<UITableViewDataSource,UITableViewDelegate>{
    UITableView * table;
    
    UIButton * fixBtn;
    FixProductDetail * fixPrdDetail;
    NSString * title;
    UIImageView *imgLines;
}

-(void)addContentView;
-(void)clickOnBut:(id)btn;

@end

@implementation FixProductDetailController

- (id)initWithProDetail: (FixProductDetail*) prjDetail{
    self = [super init];
    if (self) {
        fixPrdDetail = [[FixProductDetail alloc]init];
        fixPrdDetail.qixian =@"24个月";
        fixPrdDetail.didian =@"成都";
        fixPrdDetail.touziqidian =@"100万元";
        fixPrdDetail.fuxifangshi =@"半年付";
        fixPrdDetail.fashouriqi =@"2014-4-26";
        fixPrdDetail.zixunrenshu =@"10000";
        fixPrdDetail.shouyilv =@"15%";
        fixPrdDetail.zixun =@"中融信托与成都建发、成都市龙泉驿区政府三方签署《债权债务确认协议》，明确成都市龙泉驿区政府作为债务人向受托人履行偿付应收账款的义务；龙泉驿区财政局出具相关文件承诺安排专项资金用于债务偿还。";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addTopViewWithTitle:@"财产权信托" leftButtonTitle:@"返回" rightButtonTitle:nil];
    [self addContentView];
}

-(void)addContentView{
    UILabel * head1 = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+5, 30, 20)];
    head1.font = [UIFont systemFontOfSize:14.0];
    head1.backgroundColor = [UIColor clearColor];
    head1.textColor = [UIColor blackColor];
    head1.text = @"概况";
    [self.view addSubview:head1];
    
    UILabel * head2 = [[UILabel alloc] initWithFrame:CGRectMake(DEVICE_WIDTH-60, TITLE_HEIGHT+5, 60, 20)];
    head2.font = [UIFont systemFontOfSize:14.0];
    head2.backgroundColor = [UIColor clearColor];
    head2.textColor = [UIColor blackColor];
    head2.text = @"信托产品";
    
    [self.view addSubview:head2];
    
    imgLines =[[UIImageView alloc] initWithFrame:CGRectMake(0,TITLE_HEIGHT+30, DEVICE_WIDTH, 5)];
    [self.view addSubview:imgLines];
    UIGraphicsBeginImageContext(imgLines.frame.size);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapSquare); //端点形状
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 1); //线条粗细
    CGContextSetAllowsAntialiasing(UIGraphicsGetCurrentContext(), YES);
    CGContextSetShouldAntialias(UIGraphicsGetCurrentContext(), NO);//设置线条平滑，不需要两边像素宽
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 0.4); //颜色
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), 0, 2.5);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), DEVICE_WIDTH, 2.5);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    imgLines.image=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UILabel * fengxian = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+40, 30, 20)];
    fengxian.font = [UIFont systemFontOfSize:14.0];
    fengxian.backgroundColor = [UIColor clearColor];
    fengxian.textColor = [UIColor blackColor];
    fengxian.text = @"风险";
    [self.view addSubview:fengxian];
    
    //add rate image
    UIImageView * oneView = [[UIImageView alloc] initWithFrame:CGRectMake(30, TITLE_HEIGHT+40, 20, 20)];
    [oneView setImage:[[UIImage imageNamed:@"u56_normal.png"] resizedImageToFitInSize:CGSizeMake(20, 20) scaleIfSmaller:TRUE]];
    [self.view  addSubview:oneView];
    
    UIColor* textColor = [UIColor blackColor];
    UILabel * attrVal1View = [[UILabel alloc] initWithFrame:CGRectMake(32, TITLE_HEIGHT+40, 20, 20)];
    attrVal1View.textAlignment = NSTextAlignmentLeft;
    attrVal1View.font = [UIFont systemFontOfSize:14.0];
    attrVal1View.backgroundColor = [UIColor clearColor];
    attrVal1View.textColor = textColor;
    attrVal1View.text = @"低";
    [self.view addSubview:attrVal1View];
    
    UILabel * dangbao = [[UILabel alloc] initWithFrame:CGRectMake(60, TITLE_HEIGHT+40, 30, 20)];
    dangbao.font = [UIFont systemFontOfSize:14.0];
    dangbao.backgroundColor = [UIColor clearColor];
    dangbao.textColor = [UIColor blackColor];
    dangbao.text = @"担保";
    [self.view addSubview:dangbao];
    
    UIImageView * twoView = [[UIImageView alloc] initWithFrame:CGRectMake(90, TITLE_HEIGHT+40, 20, 20)];
    [twoView setImage:[[UIImage imageNamed:@"u59_normal.png"] resizedImageToFitInSize:CGSizeMake(20, 20) scaleIfSmaller:TRUE]];
    [self.view  addSubview:twoView];
    
    UILabel * attrVal2View = [[UILabel alloc] initWithFrame:CGRectMake(92.5, TITLE_HEIGHT+40, 20, 20)];
    attrVal2View.textAlignment = NSTextAlignmentLeft;
    attrVal2View.font = [UIFont systemFontOfSize:14.0];
    attrVal2View.backgroundColor = [UIColor clearColor];
    attrVal2View.textColor = textColor;
    attrVal2View.text = @"强";
    [self.view addSubview:attrVal2View];
    
    UILabel * diya = [[UILabel alloc] initWithFrame:CGRectMake(120, TITLE_HEIGHT+40, 30, 20)];
    diya.font = [UIFont systemFontOfSize:14.0];
    diya.backgroundColor = [UIColor clearColor];
    diya.textColor = [UIColor blackColor];
    diya.text = @"抵押";
    [self.view addSubview:diya];

    UIImageView * threeView = [[UIImageView alloc] initWithFrame:CGRectMake(150, TITLE_HEIGHT+40, 20, 20)];
    [threeView setImage:[[UIImage imageNamed:@"u62_normal.png"] resizedImageToFitInSize:CGSizeMake(20, 20) scaleIfSmaller:TRUE]];
    [self.view  addSubview:threeView];
    
    UILabel * attrVal3View = [[UILabel alloc] initWithFrame:CGRectMake(152, TITLE_HEIGHT+40, 20, 20)];
    attrVal3View.textAlignment = NSTextAlignmentLeft;
    attrVal3View.font = [UIFont systemFontOfSize:14.0];
    attrVal3View.backgroundColor = [UIColor clearColor];
    attrVal3View.textColor = textColor;
    attrVal3View.text = @"足";
    [self.view addSubview:attrVal3View];
    
    UILabel * zijinjian = [[UILabel alloc] initWithFrame:CGRectMake(180, TITLE_HEIGHT+40, 50, 20)];
    zijinjian.font = [UIFont systemFontOfSize:14.0];
    zijinjian.backgroundColor = [UIColor clearColor];
    zijinjian.textColor = [UIColor blackColor];
    zijinjian.text = @"资金监";
    [self.view addSubview:zijinjian];
    
    UIImageView * rateView = [[UIImageView alloc] initWithFrame:CGRectMake(230, TITLE_HEIGHT+40, 20, 20)];
    [rateView setImage:[[UIImage imageNamed:@"u65_normal.png"] resizedImageToFitInSize:CGSizeMake(20, 20) scaleIfSmaller:TRUE]];
    [self.view  addSubview:rateView];
    
    UILabel * attrVal4View = [[UILabel alloc] initWithFrame:CGRectMake(232, TITLE_HEIGHT+40, 20, 20)];
    attrVal4View.textAlignment = NSTextAlignmentLeft;
    attrVal4View.font = [UIFont systemFontOfSize:14.0];
    attrVal4View.backgroundColor = [UIColor clearColor];
    attrVal4View.textColor = textColor;
    attrVal4View.text = @"管";
    [self.view addSubview:attrVal4View];
    
    UILabel * cpqixian = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+80, 100, 20)];
    cpqixian.font = [UIFont systemFontOfSize:14.0];
    cpqixian.backgroundColor = [UIColor clearColor];
    cpqixian.textColor = [UIColor blackColor];
    cpqixian.text = @"产品期限: ";
    [self.view addSubview:cpqixian];
    
    UILabel * qixian = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+80, 100, 20)];
    qixian.font = [UIFont systemFontOfSize:14.0];
    qixian.backgroundColor = [UIColor clearColor];
    qixian.textColor = [UIColor blackColor];
    qixian.text = fixPrdDetail.qixian;
    [self.view addSubview:qixian];
    
    UILabel * xmsuozaidi = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+110, 100, 20)];
    xmsuozaidi.font = [UIFont systemFontOfSize:14.0];
    xmsuozaidi.backgroundColor = [UIColor clearColor];
    xmsuozaidi.textColor = [UIColor blackColor];
    xmsuozaidi.text = @"项目所在地: ";
    [self.view addSubview:xmsuozaidi];
    
    UILabel * didian = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+110, 100, 20)];
    didian.font = [UIFont systemFontOfSize:14.0];
    didian.backgroundColor = [UIColor clearColor];
    didian.textColor = [UIColor blackColor];
    didian.text = fixPrdDetail.didian;
    [self.view addSubview:didian];
    
    UILabel * touziqidian = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+140, 100, 20)];
    touziqidian.font = [UIFont systemFontOfSize:14.0];
    touziqidian.backgroundColor = [UIColor clearColor];
    touziqidian.textColor = [UIColor blackColor];
    touziqidian.text = @"投资起点: ";
    [self.view addSubview:touziqidian];
    
    UILabel * qidian = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+140, 100, 20)];
    qidian.font = [UIFont systemFontOfSize:14.0];
    qidian.backgroundColor = [UIColor clearColor];
    qidian.textColor = [UIColor blackColor];
    qidian.text = fixPrdDetail.touziqidian;
    [self.view addSubview:qidian];

    UILabel * fuxifangshi = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+170, 100, 20)];
    fuxifangshi.font = [UIFont systemFontOfSize:14.0];
    fuxifangshi.backgroundColor = [UIColor clearColor];
    fuxifangshi.textColor = [UIColor blackColor];
    fuxifangshi.text = @"付息方式: ";
    [self.view addSubview:fuxifangshi];
    
    UILabel * fuxi = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+170, 100, 20)];
    fuxi.font = [UIFont systemFontOfSize:14.0];
    fuxi.backgroundColor = [UIColor clearColor];
    fuxi.textColor = [UIColor blackColor];
    fuxi.text = fixPrdDetail.fuxifangshi;
    [self.view addSubview:fuxi];

    UILabel * fashouriqi = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+200, 100, 20)];
    fashouriqi.font = [UIFont systemFontOfSize:14.0];
    fashouriqi.backgroundColor = [UIColor clearColor];
    fashouriqi.textColor = [UIColor blackColor];
    fashouriqi.text = @"发售日期: ";
    [self.view addSubview:fashouriqi];
    
    UILabel * riqi = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+200, 100, 20)];
    riqi.font = [UIFont systemFontOfSize:14.0];
    riqi.backgroundColor = [UIColor clearColor];
    riqi.textColor = [UIColor blackColor];
    riqi.text = fixPrdDetail.fashouriqi;
    [self.view addSubview:riqi];

    UILabel * zixun = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+230, 100, 20)];
    zixun.font = [UIFont systemFontOfSize:14.0];
    zixun.backgroundColor = [UIColor clearColor];
    zixun.textColor = [UIColor redColor];
    zixun.text = fixPrdDetail.zixunrenshu;
    [self.view addSubview:zixun];
    
    UILabel * zixunw = [[UILabel alloc] initWithFrame:CGRectMake(50, TITLE_HEIGHT+230, 100, 20)];
    zixunw.font = [UIFont systemFontOfSize:14.0];
    zixunw.backgroundColor = [UIColor clearColor];
    zixunw.textColor = [UIColor blackColor];
    zixunw.text = @"人咨询过产品";
    [self.view addSubview:zixunw];
    
    UILabel * shouyilvshuoming = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+260, 100, 20)];
    shouyilvshuoming.font = [UIFont systemFontOfSize:14.0];
    shouyilvshuoming.backgroundColor = [UIColor clearColor];
    shouyilvshuoming.textColor = [UIColor blackColor];
    shouyilvshuoming.text = @"收益率说明";
    [self.view addSubview:shouyilvshuoming];
    
    UILabel * zijinyunyong = [[UILabel alloc] initWithFrame:CGRectMake(85, TITLE_HEIGHT+260, 100, 20)];
    zijinyunyong.font = [UIFont systemFontOfSize:14.0];
    zijinyunyong.backgroundColor = [UIColor clearColor];
    zijinyunyong.textColor = [UIColor blackColor];
    zijinyunyong.text = @"资金运用";
    [self.view addSubview:zijinyunyong];
    
    UILabel * fengxiankongzhi = [[UILabel alloc] initWithFrame:CGRectMake(165, TITLE_HEIGHT+260, 100, 20)];
    fengxiankongzhi.font = [UIFont systemFontOfSize:14.0];
    fengxiankongzhi.backgroundColor = [UIColor clearColor];
    fengxiankongzhi.textColor = [UIColor blackColor];
    fengxiankongzhi.text = @"风险控制";
    [self.view addSubview:fengxiankongzhi];
    
    UILabel * liangdian = [[UILabel alloc] initWithFrame:CGRectMake(245, TITLE_HEIGHT+260, 100, 20)];
    liangdian.font = [UIFont systemFontOfSize:14.0];
    liangdian.backgroundColor = [UIColor clearColor];
    liangdian.textColor = [UIColor blackColor];
    liangdian.text = @"亮点";
    [self.view addSubview:liangdian];
    
    UILabel * mingxi = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+285, DEVICE_WIDTH, 100)];
    mingxi.font = [UIFont systemFontOfSize:14.0];
    mingxi.backgroundColor = [UIColor clearColor];
    mingxi.textColor = [UIColor blackColor];
    mingxi.text = fixPrdDetail.zixun;
    mingxi.numberOfLines = 0;// 换行
    mingxi.lineBreakMode = UILineBreakModeCharacterWrap;// 换行
    [self.view addSubview:mingxi];

    UILabel * shouyilv = [[UILabel alloc] initWithFrame:CGRectMake(160, TITLE_HEIGHT+60, 100, 100)];
    shouyilv.font = [UIFont systemFontOfSize:40.0];
    shouyilv.backgroundColor = [UIColor clearColor];
    shouyilv.textColor = [UIColor redColor];
    shouyilv.text = fixPrdDetail.shouyilv;
    [self.view addSubview:shouyilv];
    
    UILabel * yuqishouyilv = [[UILabel alloc] initWithFrame:CGRectMake(250, TITLE_HEIGHT+110, 60, 20)];
    yuqishouyilv.font = [UIFont systemFontOfSize:12.0];
    yuqishouyilv.backgroundColor = [UIColor clearColor];
    yuqishouyilv.textColor = [UIColor blackColor];
    yuqishouyilv.text = @"预期收益率";
    [self.view addSubview:yuqishouyilv];
    
    //预约按钮
    UIButton * bottomLb = [[UIButton alloc] initWithFrame:CGRectMake(160, TITLE_HEIGHT+150, 150, 30)];
    bottomLb.backgroundColor = [UIColor redColor];
    [bottomLb setTitle:@"预约" forState:UIControlStateNormal];
    [self.view addSubview:bottomLb];
    
    UILabel * maijiusong = [[UILabel alloc] initWithFrame:CGRectMake(160, TITLE_HEIGHT+190, 150, 20)];
    maijiusong.font = [UIFont systemFontOfSize:12.0];
    maijiusong.backgroundColor = [UIColor clearColor];
    maijiusong.textColor = [UIColor blackColor];
    maijiusong.text = @"买就送：3000元红包";
    [self.view addSubview:maijiusong];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)clickOnBut:(id)btn{
//    UIButton * button = (UIButton*)btn;
}

-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}

@end
