//
//  FixProductDetailController.m
//  Soufu580
//
//  Created by 邓启翔 on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "FloatProductDetailController.h"
#import "UIImage+Resize.h"
#import "defines.h"
#import "FloatProductDetail.h"

@interface FloatProductDetailController ()<UITableViewDataSource,UITableViewDelegate>{
    UITableView * table;
    
    UIButton * fixBtn;
    FloatProductDetail * floatPrdDetail;
    NSString * title;
    UIImageView *imgLines;
}

-(void)addContentView;
-(void)clickOnBut:(id)btn;

@end

@implementation FloatProductDetailController

- (id)initWithProDetail: (FloatProductDetail*) prjDetail{
    self = [super init];
    if (self) {
        floatPrdDetail = [[FloatProductDetail alloc]init];
        floatPrdDetail.jingli=@"赵丹阳";
        floatPrdDetail.gongsi =@"深圳私募";
        floatPrdDetail.faxing =@"aa公司";
        floatPrdDetail.touziqidian =@"100 万元";
        floatPrdDetail.jiezhiriqi =@"2014-4-26";
        floatPrdDetail.zixunrenshu =@"10000";
        floatPrdDetail.rengoufei =@"15%";
        floatPrdDetail.guanglifei =@"15%";
        floatPrdDetail.shouyilv =@"500%";
        floatPrdDetail.xiangxijieshao =@"中融信托与成都建发、成都市龙泉驿区政府三方签署《债权债务确认协议》，明确成都市龙泉驿区政府作为债务人向受托人履行偿付应收账款的义务；龙泉驿区财政局出具相关文件承诺安排专项资金用于债务偿还。";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addTopViewWithTitle:@"财产权信托" leftButtonTitle:@"返回" rightButtonTitle:nil];
    [self addContentView];
}

-(void)addContentView{
    UILabel * head1 = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+5, 30, 20)];
    head1.font = [UIFont systemFontOfSize:14.0];
    head1.backgroundColor = [UIColor clearColor];
    head1.textColor = [UIColor blackColor];
    head1.text = @"概况";
    [self.view addSubview:head1];
    
    imgLines =[[UIImageView alloc] initWithFrame:CGRectMake(0,TITLE_HEIGHT+30, DEVICE_WIDTH, 5)];
    [self.view addSubview:imgLines];
    UIGraphicsBeginImageContext(imgLines.frame.size);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapSquare); //端点形状
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 1); //线条粗细
    CGContextSetAllowsAntialiasing(UIGraphicsGetCurrentContext(), YES);
    CGContextSetShouldAntialias(UIGraphicsGetCurrentContext(), NO);//设置线条平滑，不需要两边像素宽
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 0.4); //颜色
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), 0, 2.5);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), DEVICE_WIDTH, 2.5);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    imgLines.image=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UILabel * jingliLable = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+40, 100, 20)];
    jingliLable.font = [UIFont systemFontOfSize:14.0];
    jingliLable.backgroundColor = [UIColor clearColor];
    jingliLable.textColor = [UIColor blackColor];
    jingliLable.text = @"基金经理: ";
    [self.view addSubview:jingliLable];
    
    UILabel * jingli = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+40, 100, 20)];
    jingli.font = [UIFont systemFontOfSize:14.0];
    jingli.backgroundColor = [UIColor clearColor];
    jingli.textColor = [UIColor blackColor];
    jingli.text = floatPrdDetail.jingli;
    [self.view addSubview:jingli];
    
    UILabel * gongsiLable = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+70, 100, 20)];
    gongsiLable.font = [UIFont systemFontOfSize:14.0];
    gongsiLable.backgroundColor = [UIColor clearColor];
    gongsiLable.textColor = [UIColor blackColor];
    gongsiLable.text = @"基金公司: ";
    [self.view addSubview:gongsiLable];
    
    UILabel * gongsi = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+70, 100, 20)];
    gongsi.font = [UIFont systemFontOfSize:14.0];
    gongsi.backgroundColor = [UIColor clearColor];
    gongsi.textColor = [UIColor blackColor];
    gongsi.text = floatPrdDetail.gongsi;
    [self.view addSubview:gongsi];
    
    UILabel * faxingLable = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+100, 100, 20)];
    faxingLable.font = [UIFont systemFontOfSize:14.0];
    faxingLable.backgroundColor = [UIColor clearColor];
    faxingLable.textColor = [UIColor blackColor];
    faxingLable.text = @"发行公司: ";
    [self.view addSubview:faxingLable];
    
    UILabel * faxing = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+100, 100, 20)];
    faxing.font = [UIFont systemFontOfSize:14.0];
    faxing.backgroundColor = [UIColor clearColor];
    faxing.textColor = [UIColor blackColor];
    faxing.text = floatPrdDetail.faxing;
    [self.view addSubview:faxing];
    
    UILabel * touziqidianLable = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+130, 100, 20)];
    touziqidianLable.font = [UIFont systemFontOfSize:14.0];
    touziqidianLable.backgroundColor = [UIColor clearColor];
    touziqidianLable.textColor = [UIColor blackColor];
    touziqidianLable.text = @"投资起点: ";
    [self.view addSubview:touziqidianLable];
    
    UILabel * qidian = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+130, 100, 20)];
    qidian.font = [UIFont systemFontOfSize:14.0];
    qidian.backgroundColor = [UIColor clearColor];
    qidian.textColor = [UIColor blackColor];
    qidian.text = floatPrdDetail.touziqidian;
    [self.view addSubview:qidian];
    
    UILabel * jiezhiriqiLable = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+160, 100, 20)];
    jiezhiriqiLable.font = [UIFont systemFontOfSize:14.0];
    jiezhiriqiLable.backgroundColor = [UIColor clearColor];
    jiezhiriqiLable.textColor = [UIColor blackColor];
    jiezhiriqiLable.text = @"截止日期: ";
    [self.view addSubview:jiezhiriqiLable];
    
    UILabel * riqi = [[UILabel alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+160, 100, 20)];
    riqi.font = [UIFont systemFontOfSize:14.0];
    riqi.backgroundColor = [UIColor clearColor];
    riqi.textColor = [UIColor blackColor];
    riqi.text = floatPrdDetail.jiezhiriqi;
    [self.view addSubview:riqi];
    
    UILabel * rengoufeiLable = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+200, 50, 20)];
    rengoufeiLable.font = [UIFont systemFontOfSize:14.0];
    rengoufeiLable.backgroundColor = [UIColor clearColor];
    rengoufeiLable.textColor = [UIColor blackColor];
    rengoufeiLable.text = @"认购费:";
    [self.view addSubview:rengoufeiLable];
    
    UILabel * rengoufei = [[UILabel alloc] initWithFrame:CGRectMake(55, TITLE_HEIGHT+200, 35, 20)];
    rengoufei.font = [UIFont systemFontOfSize:14.0];
    rengoufei.backgroundColor = [UIColor clearColor];
    rengoufei.textColor = [UIColor redColor];
    rengoufei.text = floatPrdDetail.rengoufei;
    [self.view addSubview:rengoufei];
    
    UILabel * guanglifeiLable = [[UILabel alloc] initWithFrame:CGRectMake(95, TITLE_HEIGHT+200, 50, 20)];
    guanglifeiLable.font = [UIFont systemFontOfSize:14.0];
    guanglifeiLable.backgroundColor = [UIColor clearColor];
    guanglifeiLable.textColor = [UIColor blackColor];
    guanglifeiLable.text = @"管理费:";
    [self.view addSubview:guanglifeiLable];
    
    UILabel * guanglifei = [[UILabel alloc] initWithFrame:CGRectMake(145, TITLE_HEIGHT+200, 35, 20)];
    guanglifei.font = [UIFont systemFontOfSize:14.0];
    guanglifei.backgroundColor = [UIColor clearColor];
    guanglifei.textColor = [UIColor redColor];
    guanglifei.text = floatPrdDetail.guanglifei;
    [self.view addSubview:guanglifei];
    
    UILabel * zixun = [[UILabel alloc] initWithFrame:CGRectMake(190, TITLE_HEIGHT+200, 40, 20)];
    zixun.font = [UIFont systemFontOfSize:14.0];
    zixun.backgroundColor = [UIColor clearColor];
    zixun.textColor = [UIColor redColor];
    zixun.text = floatPrdDetail.zixunrenshu;
    [self.view addSubview:zixun];
    
    UILabel * zixunw = [[UILabel alloc] initWithFrame:CGRectMake(230, TITLE_HEIGHT+200, 100, 20)];
    zixunw.font = [UIFont systemFontOfSize:14.0];
    zixunw.backgroundColor = [UIColor clearColor];
    zixunw.textColor = [UIColor blackColor];
    zixunw.text = @"人咨询过产品";
    [self.view addSubview:zixunw];
    
    UILabel * shouyilvshuoming = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+230, 100, 20)];
    shouyilvshuoming.font = [UIFont systemFontOfSize:12.0];
    shouyilvshuoming.backgroundColor = [UIColor clearColor];
    shouyilvshuoming.textColor = [UIColor blackColor];
    shouyilvshuoming.text = @"基金经理介绍";
    [self.view addSubview:shouyilvshuoming];
    
    UILabel * zijinyunyong = [[UILabel alloc] initWithFrame:CGRectMake(85, TITLE_HEIGHT+230, 100, 20)];
    zijinyunyong.font = [UIFont systemFontOfSize:12.0];
    zijinyunyong.backgroundColor = [UIColor clearColor];
    zijinyunyong.textColor = [UIColor blackColor];
    zijinyunyong.text = @"历史业绩概述";
    [self.view addSubview:zijinyunyong];
    
    UILabel * fengxiankongzhi = [[UILabel alloc] initWithFrame:CGRectMake(165, TITLE_HEIGHT+230, 100, 20)];
    fengxiankongzhi.font = [UIFont systemFontOfSize:12.0];
    fengxiankongzhi.backgroundColor = [UIColor clearColor];
    fengxiankongzhi.textColor = [UIColor blackColor];
    fengxiankongzhi.text = @"产品亮点";
    [self.view addSubview:fengxiankongzhi];
    
    UILabel * mingxi = [[UILabel alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+245, DEVICE_WIDTH, 100)];
    mingxi.font = [UIFont systemFontOfSize:14.0];
    mingxi.backgroundColor = [UIColor clearColor];
    mingxi.textColor = [UIColor blackColor];
    mingxi.text = floatPrdDetail.xiangxijieshao;
    mingxi.numberOfLines = 0;// 换行
    mingxi.lineBreakMode = UILineBreakModeCharacterWrap;// 换行
    [self.view addSubview:mingxi];
    
    UILabel * shouyilvLable = [[UILabel alloc] initWithFrame:CGRectMake(150, TITLE_HEIGHT+40, 150, 20)];
    shouyilvLable.font = [UIFont systemFontOfSize:14.0];
    shouyilvLable.backgroundColor = [UIColor clearColor];
    shouyilvLable.textColor = [UIColor redColor];
    shouyilvLable.text = @"历史累计收益";
    [self.view addSubview:shouyilvLable];
    
    UILabel * shouyilv = [[UILabel alloc] initWithFrame:CGRectMake(150, TITLE_HEIGHT+60, 120, 80)];
    shouyilv.font = [UIFont systemFontOfSize:40.0];
    shouyilv.backgroundColor = [UIColor clearColor];
    shouyilv.textColor = [UIColor redColor];
    shouyilv.text = floatPrdDetail.shouyilv;
    [self.view addSubview:shouyilv];
    
    UILabel * yuqishouyilv = [[UILabel alloc] initWithFrame:CGRectMake(255, TITLE_HEIGHT+110, 60, 20)];
    yuqishouyilv.font = [UIFont systemFontOfSize:12.0];
    yuqishouyilv.backgroundColor = [UIColor clearColor];
    yuqishouyilv.textColor = [UIColor blackColor];
    yuqishouyilv.text = @"预期收益率";
    [self.view addSubview:yuqishouyilv];
    
    //预约按钮
    UIButton * bottomLb = [[UIButton alloc] initWithFrame:CGRectMake(150, TITLE_HEIGHT+130, 160, 30)];
    bottomLb.backgroundColor = [UIColor redColor];
    [bottomLb setTitle:@"预约" forState:UIControlStateNormal];
    [self.view addSubview:bottomLb];
    
    UILabel * maijiusong = [[UILabel alloc] initWithFrame:CGRectMake(160, TITLE_HEIGHT+160, 150, 20)];
    maijiusong.font = [UIFont systemFontOfSize:12.0];
    maijiusong.backgroundColor = [UIColor clearColor];
    maijiusong.textColor = [UIColor blackColor];
    maijiusong.text = @"买就送：3000元红包";
    [self.view addSubview:maijiusong];
    
    UIImageView *lines =[[UIImageView alloc] initWithFrame:CGRectMake(0,TITLE_HEIGHT+195, DEVICE_WIDTH, 5)];
    [self.view addSubview:lines];
    UIGraphicsBeginImageContext(lines.frame.size);
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapSquare); //端点形状
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 1); //线条粗细
    CGContextSetAllowsAntialiasing(UIGraphicsGetCurrentContext(), YES);
    CGContextSetShouldAntialias(UIGraphicsGetCurrentContext(), NO);//设置线条平滑，不需要两边像素宽
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 0.4); //颜色
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), 0, 2.5);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), DEVICE_WIDTH, 2.5);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    lines.image=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)clickOnBut:(id)btn{
    //    UIButton * button = (UIButton*)btn;
}

-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}

@end
