//
//  FixProductDetailController.h
//  Soufu580
//
//  Created by 邓启翔 on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "FixProductDetail.h"

@interface FixProductDetailController : BaseVC
-(id)initWithProDetail:(FixProductDetail *) prjDetail;
@end
