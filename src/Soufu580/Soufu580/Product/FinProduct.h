//
//  FinProduct.h
//  Soufu580
//
//  Created by  apple on 14-4-17.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FinProduct : NSObject{
    NSString * _title;
    NSString * _bottomLine;
    NSString * _asked;
    NSString * _subTitle;
    NSString * _contentA;
    NSString * _contentB;
    NSString * _contentC;
    NSString * _contentD;
    NSString * _contentF;
}

@property (nonatomic,retain) NSString * title;
@property (nonatomic,retain) NSString * subTitle;
@property (nonatomic,retain) NSString * bottomLine;
@property (nonatomic,retain) NSString * asked;
@property (nonatomic,retain) NSString * contentA;
@property (nonatomic,retain) NSString * contentB;
@property (nonatomic,retain) NSString * contentC;
@property (nonatomic,retain) NSString * contentD;
@property (nonatomic,retain) NSString * contentF;

@end
