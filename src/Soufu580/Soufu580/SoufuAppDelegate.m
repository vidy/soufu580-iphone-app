//
//  SoufuAppDelegate.m
//  Soufu580
//
//  Created by vidy on 14-4-16.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "SoufuAppDelegate.h"
#import "MainViewController.h"
#import "Main2ViewController.h"
#import "ProductViewController.h"
#import "MyVC.h"
#import "SettingVC.h"
#import "StrategyVC.h"
#import "UIImage+Resize.h"
#import "defines.h"


@implementation SoufuAppDelegate


@synthesize window = _window;
@synthesize tarBarController = _tarBarController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //首页
    NSLog(@"device height %d \n..........",DEVICE_HEIGHT);
    
    //首页
    Main2ViewController* main2ViewController = [[Main2ViewController alloc]init];
    UINavigationController *main2Nav = [[UINavigationController alloc]initWithRootViewController:main2ViewController];
    main2Nav.navigationBarHidden = TRUE;
    
    //产品
    ProductViewController* productViewController = [[ProductViewController alloc]init];
    UINavigationController *productNav = [[UINavigationController alloc]initWithRootViewController:productViewController];
    productNav.navigationBarHidden = TRUE;
    
    //攻略
    StrategyVC* strategyVC = [[StrategyVC alloc]init];
    UINavigationController *strategyNav = [[UINavigationController alloc]initWithRootViewController:strategyVC];
    strategyNav.navigationBarHidden = TRUE;
    
    //我的投资
    MyVC* myVC = [[MyVC alloc]init];
    UINavigationController *myNav = [[UINavigationController alloc]initWithRootViewController:myVC];
    myNav.navigationBarHidden = TRUE;
    
    //更多
    SettingVC* setVC = [[SettingVC alloc]init];
    UINavigationController *setNav = [[UINavigationController alloc]initWithRootViewController:setVC];
    setNav.navigationBarHidden = TRUE;
    
    self.tarBarController = [[UITabBarController alloc] init];
    //定制化tabbar
    [[[self tarBarController] tabBar]
     setBackgroundImage:[[UIImage imageNamed:@"tabarView"] resizedImageToFitInSize:CGSizeMake(DEVICE_WIDTH, BOTTOM_HEIGHT) scaleIfSmaller:TRUE]];
    
    [[[self tarBarController] tabBar]
     setSelectionIndicatorImage:[[UIImage imageNamed:@"tabarSelected"] resizedImageToFitInSize:CGSizeMake(64, BOTTOM_HEIGHT) scaleIfSmaller:TRUE]];
    
   // self.tarBarController.delegate=self;
    self.tarBarController.viewControllers = [NSArray arrayWithObjects:main2Nav,productNav,strategyNav,myNav,setNav, nil];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.rootViewController = self.tarBarController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
