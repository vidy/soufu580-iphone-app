//
//  PwdManageVC.m
//  Soufu580
//
//  Created by 卞东雨 on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "PwdManageVC.h"
#import "UIImage+Resize.h"
#import "SettingAbs.h"
#import "LoginVC.h"
#import "defines.h"
#import "RegisterVC.h"

@interface PwdManageVC ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray * tableArr;
}

@end

@implementation PwdManageVC

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self addTopViewWithTitle:@"修改密码" leftButtonTitle:@"返回" rightButtonTitle:nil];
    
    UILabel * telLbl = [[UILabel alloc] initWithFrame:CGRectMake(20,TITLE_HEIGHT+20 , 100, 20)];
    telLbl.backgroundColor = [UIColor clearColor];
    telLbl.font = [UIFont boldSystemFontOfSize:14.0];
    telLbl.textAlignment=NSTextAlignmentRight;
    telLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    telLbl.text = @"请输入旧密码:";
    [self.view addSubview:telLbl];
    
    UITextField * telText = [[UITextField alloc] initWithFrame:CGRectMake(125, TITLE_HEIGHT+20 , 160, 20)];
    telText.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:telText];
    
    UILabel * pwdLbl = [[UILabel alloc] initWithFrame:CGRectMake(20,TITLE_HEIGHT+50 , 100, 20)];
    pwdLbl.backgroundColor = [UIColor clearColor];
    pwdLbl.font = [UIFont boldSystemFontOfSize:14.0];
    pwdLbl.textAlignment=NSTextAlignmentRight;
    pwdLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    pwdLbl.text = @"请输入新密码:";
    [self.view addSubview:pwdLbl];
    
    UITextField * pwdText = [[UITextField alloc] initWithFrame:CGRectMake(125, TITLE_HEIGHT+50 , 160, 20)];
    pwdText.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pwdText];
    
    UILabel * pwdLbl2 = [[UILabel alloc] initWithFrame:CGRectMake(5,TITLE_HEIGHT+80 , 120, 20)];
    pwdLbl2.backgroundColor = [UIColor clearColor];
    pwdLbl2.font = [UIFont boldSystemFontOfSize:14.0];
    pwdLbl2.textAlignment=NSTextAlignmentRight;
    pwdLbl2.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    pwdLbl2.text = @"再次输入新密码:";
    [self.view addSubview:pwdLbl2];
    
    UITextField * pwdText2 = [[UITextField alloc] initWithFrame:CGRectMake(125, TITLE_HEIGHT+80 , 160, 20)];
    pwdText2.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pwdText2];
    
    UIButton *lgnBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, TITLE_HEIGHT+110, 220, 30)];
    [lgnBtn setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateNormal];
    [lgnBtn setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateSelected];
    [lgnBtn setTitle:@"确定" forState:UIControlStateNormal];
    [lgnBtn.titleLabel setFont:[UIFont systemFontOfSize:20.0]];
    [lgnBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:lgnBtn];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}
-(void)clickOnRegBtn{
    RegisterVC* regVc = [[RegisterVC alloc]init];
    [self.navigationController pushViewController:regVc animated:TRUE];
}


@end
