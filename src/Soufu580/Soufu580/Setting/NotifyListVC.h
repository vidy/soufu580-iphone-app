//
//  NotifyListVC.h
//  Soufu580
//
//  Created by vidy on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "BaseVC.h"
#import "UIFolderTableView.h"

@interface NotifyListVC : BaseVC

@property (strong, nonatomic) NSArray *cates;
@property (strong, nonatomic) IBOutlet UIFolderTableView *tableView;

@end
