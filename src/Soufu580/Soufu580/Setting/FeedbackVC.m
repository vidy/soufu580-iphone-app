//
//  FeedbackVC.m
//  Soufu580
//
//  Created by 卞东雨 on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "FeedbackVC.h"
#import "LoginVC.h"
#import "defines.h"
#import "RegisterVC.h"
#import "QuartzCore/QuartzCore.h"

@interface FeedbackVC ()

@end

@implementation FeedbackVC

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    [self addTopViewWithTitle:@"反馈建议" leftButtonTitle:@"返回" rightButtonTitle:nil];
    
    UITextView * noteText = [[UITextView alloc] initWithFrame:CGRectMake(30, TITLE_HEIGHT+20 , 260, 200)];
    noteText.backgroundColor = [UIColor whiteColor];
    noteText.font = [UIFont fontWithName:@"Arial" size:14.0];
    noteText.textColor = [UIColor redColor];
    noteText.text = @"请输入你的宝贵建议";
    noteText.returnKeyType = UIReturnKeyDefault;
    noteText.keyboardType = UIKeyboardTypeDefault;
    noteText.scrollEnabled = YES;
    noteText.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:noteText];
    
    UIButton *lgnBtn = [[UIButton alloc] initWithFrame:CGRectMake(80, TITLE_HEIGHT+190, 120, 30)];
    [lgnBtn setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateNormal];
    [lgnBtn setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateSelected];
    [lgnBtn setTitle:@"提交" forState:UIControlStateNormal];
    [lgnBtn.titleLabel setFont:[UIFont systemFontOfSize:20.0]];
    [lgnBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:lgnBtn];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}
@end
