//
//  SettingAbs.h
//  Soufu580
//
//  Created by 卞东雨 on 14-4-23.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SettingAbs : NSObject
{
    NSString * _abs;
    NSString * _title;
    NSInteger _height;
    NSInteger _fontSize;
}

@property (nonatomic, retain) NSString * abs;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, readwrite) NSInteger height;
@property (nonatomic, readwrite) NSInteger fontSize;

@end
