//
//  AccountInfoVC.m
//  Soufu580
//
//  Created by 卞东雨 on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "AccountInfoVC.h"
#import "SettingAbs.h"
#import "LoginVC.h"
#import "defines.h"
#import "RegisterVC.h"

@interface AccountInfoVC ()

@end

@implementation AccountInfoVC
- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self addTopViewWithTitle:@"账户信息" leftButtonTitle:@"返回" rightButtonTitle:nil];
    
    UILabel * nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(20,TITLE_HEIGHT+20 , 200, 20)];
    nameLbl.backgroundColor = [UIColor clearColor];
    nameLbl.font = [UIFont boldSystemFontOfSize:14.0];
    nameLbl.textAlignment=NSTextAlignmentLeft;
    nameLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    nameLbl.text = @"姓名:罗一";
    [self.view addSubview:nameLbl];
    
    UILabel * emailLbl = [[UILabel alloc] initWithFrame:CGRectMake(20,TITLE_HEIGHT+50 , 200, 20)];
    emailLbl.backgroundColor = [UIColor clearColor];
    emailLbl.font = [UIFont boldSystemFontOfSize:14.0];
    emailLbl.textAlignment=NSTextAlignmentLeft;
    emailLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    emailLbl.text = @"邮箱:luoyi@sofu580.com";
    [self.view addSubview:emailLbl];
    
    UILabel * telLbl = [[UILabel alloc] initWithFrame:CGRectMake(20,TITLE_HEIGHT+80 , 200, 20)];
    telLbl.backgroundColor = [UIColor clearColor];
    telLbl.font = [UIFont boldSystemFontOfSize:14.0];
    telLbl.textAlignment=NSTextAlignmentLeft;
    telLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    telLbl.text = @"手机:18966668888";
    [self.view addSubview:telLbl];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}
@end
