//
//  NotifyCellVC.m
//  Soufu580
//
//  Created by vidy on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "NotifyCellVC.h"

@interface NotifyCellVC ()

@end

@implementation NotifyCellVC
@synthesize titleLabel,arrowImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0 , 100, 20)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
        self.titleLabel.textAlignment=NSTextAlignmentRight;
        self.titleLabel.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        self.titleLabel.text = @"邮箱:";
        [self addSubview:self.titleLabel];
        
        self.arrowImageView = [[UIImageView alloc]initWithFrame:CGRectMake(290, 10, 20, 20)];
        [self.arrowImageView setImage:[UIImage imageNamed:@"UpAccessory.png"]];
        [self addSubview:self.arrowImageView];
        
        

        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)changeArrowWithUp:(BOOL)up
{
    if (up) {
        self.arrowImageView.image = [UIImage imageNamed:@"UpAccessory.png"];
    }else
    {
        self.arrowImageView.image = [UIImage imageNamed:@"DownAccessory.png"];
    }
}
@end
