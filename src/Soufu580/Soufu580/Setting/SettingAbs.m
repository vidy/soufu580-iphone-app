//
//  SettingAbs.m
//  Soufu580
//
//  Created by 卞东雨 on 14-4-23.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "SettingAbs.h"

@implementation SettingAbs
@synthesize abs = _abs;
@synthesize title = _title;
@synthesize height = _height;
@synthesize fontSize = _fontSize;
@end
