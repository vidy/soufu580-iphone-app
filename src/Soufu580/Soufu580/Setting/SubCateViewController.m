//
//  SubCateViewController.m
//  Soufu580
//
//  Created by vidy on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "SubCateViewController.h"

@interface SubCateViewController ()
@property (strong, nonatomic)  UILabel *label_Info;

@end

@implementation SubCateViewController
@synthesize label_Info;
@synthesize Info;
-(void)DisplayInfo:(NSString *)Info
{
    //    label_Info.numberOfLines = 0;
    //    CGSize size = [Info sizeWithFont:label_Info.font constrainedToSize:CGSizeMake(320, MAXFLOAT)];
    //    if(size.height < label_Info.frame.size.height)
    //    {
    //        size = CGSizeMake(320, label_Info.frame.size.height);
    //    }
    //    [label_Info setFrame:CGRectMake(0, 0, 320, size.height)];
    //    [self.view setFrame:CGRectMake(0, 0, 320, size.height)];
    //    label_Info.text = Info;
}


#pragma ViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    label_Info = [[UILabel alloc] initWithFrame:CGRectMake(0,0 , 100, 20)];
    label_Info.backgroundColor = [UIColor clearColor];
    label_Info.font = [UIFont boldSystemFontOfSize:14.0];
    label_Info.textAlignment=NSTextAlignmentRight;
    label_Info.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    label_Info.text = @"邮箱:";
    
    
    
    label_Info.numberOfLines = 0;
    CGSize size = [Info sizeWithFont:label_Info.font constrainedToSize:CGSizeMake(320, MAXFLOAT)];
    if(size.height < label_Info.frame.size.height)
    {
        size = CGSizeMake(320, label_Info.frame.size.height);
    }
    [label_Info setFrame:CGRectMake(0, 0, 320, size.height)];
    [self.view setFrame:CGRectMake(0, 0, 320, size.height)];
    label_Info.text = Info;
    [self.view addSubview:label_Info];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
