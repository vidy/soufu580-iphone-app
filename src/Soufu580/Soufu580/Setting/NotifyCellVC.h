//
//  NotifyCellVC.h
//  Soufu580
//
//  Created by vidy on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "BaseVC.h"

@interface NotifyCellVC : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UIImageView *arrowImageView;
- (void)changeArrowWithUp:(BOOL)up;

@end
