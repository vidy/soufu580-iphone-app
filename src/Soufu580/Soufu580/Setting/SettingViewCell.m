//
//  SettingViewCell.m
//  Soufu580
//
//  Created by 卞东雨 on 14-4-20.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "SettingViewCell.h"
#import "UIImage+Resize.h"
#import "StrategyAbs.h"
#import "SettingAbs.h"


NSString * kSettingCellIdentifier = @"SettingCellIdentifier";
NSInteger kSettingCellHeight = 20;
@interface SettingViewCell () {
    SettingAbs * _abs;
    UILabel * titleLb;
 
}


@property (nonatomic, retain) SettingAbs * abs;

- (void)flushCellView;

@end



@implementation SettingViewCell;

@synthesize abs = _abs;

- (id)initWithSettingAbs:(SettingAbs *)abs{
    
    self.abs = abs;
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kSettingCellIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        
        
      
        if (abs.height == 0) {
            abs.height = kSettingCellHeight;
        }
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 5.0, 320, abs.height)];
        titleLb.backgroundColor = [UIColor clearColor];
        titleLb.font = [UIFont boldSystemFontOfSize:abs.fontSize];
        titleLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        titleLb.tag = 1002;
        [self addSubview:titleLb];
        
    }
    
    [self flushCellView];
    
    return self;
}

- (id)updateSettingAbs:(SettingAbs *)abs{
    self.abs = abs;
    
    [self flushCellView];
    
    return self;
}

-(void)flushCellView{
    
    /*for (UIView * subView in self.subviews) {
     if (subView.tag>100) {
     [subView removeFromSuperview];
     }
     }*/
    
    //add textlabel
    
    // set the logo image
    
    titleLb.text = [NSString stringWithFormat:@"%@",self.abs.title];
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
