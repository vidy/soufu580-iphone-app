//
//  SettingViewCell.h
//  Soufu580
//
//  Created by 卞东雨 on 14-4-20.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingAbs.h"

@interface SettingViewCell : UITableViewCell
- (id)initWithSettingAbs:(SettingAbs*)abs;
- (id)updateSettingAbs:(SettingAbs*)abs;
@end

extern NSString * kSettingCellIdentifier;
extern NSInteger kSettingCellHeight;