//
//  AboutVC.m
//  Soufu580
//
//  Created by 卞东雨 on 14-4-27.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "AboutVC.h"
#import "LoginVC.h"
#import "defines.h"
#import "RegisterVC.h"

@interface AboutVC ()

@end

@implementation AboutVC

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTopViewWithTitle:@"关于" leftButtonTitle:@"返回" rightButtonTitle:nil];
    
    UILabel * aboutLbl = [[UILabel alloc] initWithFrame:CGRectMake(20,TITLE_HEIGHT+20 , 280, 20)];
    aboutLbl.backgroundColor = [UIColor clearColor];
    aboutLbl.font = [UIFont boldSystemFontOfSize:14.0];
    aboutLbl.textAlignment=NSTextAlignmentLeft;
    aboutLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    aboutLbl.text = @"关于本软件";
    [self.view addSubview:aboutLbl];
    
    UILabel * aboutLbl1 = [[UILabel alloc] initWithFrame:CGRectMake(20,TITLE_HEIGHT+50 , 280, 30)];
    aboutLbl1.backgroundColor = [UIColor clearColor];
    aboutLbl1.font = [UIFont boldSystemFontOfSize:18.0];
    aboutLbl1.textAlignment=NSTextAlignmentCenter;
    aboutLbl1.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    aboutLbl1.text = @"搜富580手机客户端";
    [self.view addSubview:aboutLbl1];
    
    UILabel * aboutLbl2 = [[UILabel alloc] initWithFrame:CGRectMake(20,TITLE_HEIGHT+100 , 280, 20)];
    aboutLbl2.backgroundColor = [UIColor clearColor];
    aboutLbl2.font = [UIFont boldSystemFontOfSize:14.0];
    aboutLbl2.textAlignment=NSTextAlignmentCenter;
    aboutLbl2.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    aboutLbl2.text = @"版本号：1.0.0";
    [self.view addSubview:aboutLbl2];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}
@end
