//
//  SettingVC.m
//  Soufu580
//
//  Created by  apple on 14-4-18.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "SettingVC.h"
#import "UIImage+Resize.h"
#import "StrategyAbs.h"
#import "SettingViewCell.h"
#import "SettingAbs.h"
#import "PwdManageVC.h"
#import "NotifyListVC.h"
#import "AccountInfoVC.h"
#import "AboutVC.h"
#import "FeedbackVC.h"

@interface SettingVC ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray * tableArr;
}

@end

@implementation SettingVC

- (id)init {
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"更多" image:nil tag:0];
        
        [[self tabBarItem] setSelectedImage:[[UIImage imageNamed:@"jiezhangButSel"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        [[self tabBarItem] setImage:[[UIImage imageNamed:@"jiezhangBut"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        
        
        [[self tabBarItem] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                                   [UIFont systemFontOfSize:12.0], NSFontAttributeName,
                                                   nil] forState:UIControlStateNormal];
    }
    tableArr = [[NSMutableArray alloc]init];
    SettingAbs * rowAbs = [[SettingAbs alloc] init];
    rowAbs.title = @"网站：www.sofu580.com 投资热线：400-8250-580";
    rowAbs.fontSize = 12;
    [tableArr addObject:rowAbs];
    
    SettingAbs * updatePwd = [[SettingAbs alloc] init];
    updatePwd.title = @"修改密码";
    updatePwd.fontSize = 14;
    
    [tableArr addObject:updatePwd];
    
    SettingAbs * myAccount = [[SettingAbs alloc] init];
    myAccount.title = @"账户信息";
    myAccount.fontSize = 14;
    
    [tableArr addObject:myAccount];
    
    SettingAbs * message = [[SettingAbs alloc] init];
    message.title = @"消息通知";
    message.fontSize = 14;
    
    [tableArr addObject:message];
    
    SettingAbs * about = [[SettingAbs alloc] init];
    about.title = @"关于";
    about.fontSize = 14;
    
    [tableArr addObject:about];
    
    
    SettingAbs * space = [[SettingAbs alloc] init];
    space.title = @" ";
    space.fontSize = 14;
    space.height = 80;
    
    [tableArr addObject:space];
    
    SettingAbs * feedback = [[SettingAbs alloc] init];
    feedback.title = @"反馈建议";
    feedback.fontSize = 14;
    
    [tableArr addObject:feedback];
    
    SettingAbs * scoring = [[SettingAbs alloc] init];
    scoring.title = @"打分评价";
    scoring.fontSize = 14;
    
    [tableArr addObject:scoring];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addTopViewWithTitle:@"更多" leftButtonTitle:nil rightButtonTitle:nil];
    UITableView* settingView = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 55.0, 320.0, 370.0) style:UITableViewStylePlain];
    settingView.backgroundColor = [UIColor clearColor];
    settingView.bounces = NO;
    settingView.delegate = self;
    settingView.dataSource = self;
    [self.view addSubview:settingView];
    //设置网站信息
    
}
#pragma mark -settingView 设置处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [tableArr count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SettingAbs * setting  = [tableArr objectAtIndex:indexPath.row];
    SettingViewCell * cell = [tableView dequeueReusableCellWithIdentifier:kSettingCellIdentifier];
    if (cell==nil) {
        cell = [[SettingViewCell alloc] initWithSettingAbs:setting];
        cell.userInteractionEnabled = YES;
    }else{
        [cell updateSettingAbs:setting];
    }
    
    return cell;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog([NSString stringWithFormat:@" an l :%d",indexPath.row]);
    //修改密码
    if (indexPath.row == 1) {
        PwdManageVC * pwdVC;
        if (pwdVC == nil) {
            pwdVC = [[PwdManageVC alloc] init];
        }
        
        [self.navigationController pushViewController:pwdVC animated:TRUE];
    }
    else if (indexPath.row == 3){
        NotifyListVC* notifyVC = [[NotifyListVC alloc]init];
        [self.navigationController pushViewController:notifyVC animated:TRUE];
    }
    else if (indexPath.row == 2){
        AccountInfoVC * act;
        if (act == nil) {
            act = [[AccountInfoVC alloc] init];
        }
        [self.navigationController pushViewController:act animated:TRUE];
    }
    else if (indexPath.row == 4){
        AboutVC * about;
        if (about == nil) {
            about = [[AboutVC alloc] init];
        }
        [self.navigationController pushViewController:about animated:TRUE];
    }
    else if (indexPath.row == 6){
        FeedbackVC * feedback;
        if (feedback == nil) {
            feedback = [[FeedbackVC alloc] init];
        }
        [self.navigationController pushViewController:feedback animated:TRUE];
    }
    else if (indexPath.row == 7){
        NSString * app_id = @"";
        NSString * appstoreUrlString = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?mt=8&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software&id=";
        NSString * appstoreUrl = [NSString stringWithFormat:@"%@,%@",appstoreUrlString, app_id];
        
        NSURL * url = [NSURL URLWithString:appstoreUrl];
        
        if ([[UIApplication sharedApplication] canOpenURL:url])
        {
            [[UIApplication sharedApplication] openURL:url];
        }
        else
        {
            NSLog(@"can not open");
        }
    }
}

@end
