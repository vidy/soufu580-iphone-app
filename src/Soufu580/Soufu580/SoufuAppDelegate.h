//
//  SoufuAppDelegate.h
//  Soufu580
//
//  Created by vidy on 14-4-16.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "ProductViewController.h"

@interface SoufuAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong,nonatomic) UITabBarController *tarBarController;

@end
