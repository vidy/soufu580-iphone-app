//
//  UIImage+UIImage_Resize.h
//  AgileDriver
//
//  Created by  apple on 13-3-23.
//  Copyright (c) 2013年 junyun. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIImage(ResizeCategory)
-(UIImage*)resizedImageToSize:(CGSize)dstSize;
-(UIImage*)resizedImageToFitInSize:(CGSize)boundingSize scaleIfSmaller:(BOOL)scale;
@end