//
//  Main2ViewController.m
//  Soufu580
//
//  Created by vidy on 14-4-19.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "Main2ViewController.h"
#import "UIImage+Resize.h"
#import "SGFocusImageItem.h"
#import "SGFocusImageFrame.h"
#import "defines.h"
#import "LoginVC.h"

@interface Main2ViewController ()

@end

@implementation Main2ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"首页" image:nil tag:0];
       [[self tabBarItem] setSelectedImage:[[UIImage imageNamed:@"jiezhangButSel"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        [[self tabBarItem] setImage:[[UIImage imageNamed:@"jiezhangBut"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        
        
        [[self tabBarItem] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                                   [UIFont systemFontOfSize:12.0], NSFontAttributeName,
                                                   nil] forState:UIControlStateNormal];
    }
    return self;
}
-(void) addHeader{
    //背景
    UIImageView* bgImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT) ];
    //[bgImgView setImage:[UIImage  imageNamed:@"bg_640x960.png"]];
    [bgImgView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:bgImgView];
    //Head
    UIImageView* headImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, TITLE_HEIGHT)];
    [headImgView setImage:[UIImage imageNamed:@"hengtiaolang_tltle.png"]];
    [self.view addSubview:headImgView];
    //Logo
    UIImageView* logoImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 30, 60, 60)];
    [logoImgView setImage:[[UIImage imageNamed:@"logo.png"] resizedImageToSize:CGSizeMake(60, 60)]];
    [headImgView addSubview:logoImgView];
    //LoginInfo
    /*UILabel * loginLbl = [[UILabel alloc] initWithFrame:CGRectMake(40, 30, 30, 20)];
    loginLbl.textAlignment = NSTextAlignmentLeft;
    loginLbl.font = [UIFont systemFontOfSize:10.0];
    loginLbl.backgroundColor = [UIColor clearColor];
    loginLbl.textColor = [UIColor whiteColor];
    loginLbl.text = @"未登录";
    
    
    [headImgView addSubview:loginLbl];
      */
    
    
    UIButton *loginBtn = [[UIButton alloc] initWithFrame:CGRectMake(40, 30, 30, 20)];
    [loginBtn setTitle:@"未登录" forState:UIControlStateNormal];
    [loginBtn.titleLabel setTextAlignment:NSTextAlignmentLeft];
    [loginBtn.titleLabel setFont:[UIFont systemFontOfSize:10.0]];
    [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(clickOnLoginBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    
    
    UILabel * telLbl = [[UILabel alloc] initWithFrame:CGRectMake(80, 30, 200, 20)];
    telLbl.textAlignment = NSTextAlignmentCenter;
    telLbl.font = [UIFont systemFontOfSize:15.0];
    telLbl.backgroundColor = [UIColor clearColor];
    telLbl.textColor = [UIColor whiteColor];
    telLbl.text = @"投资热线: 400-8250-580";
    [headImgView addSubview:telLbl];
    
    UILabel * locationLbl = [[UILabel alloc] initWithFrame:CGRectMake(280, 30, 30, 20)];
    locationLbl.textAlignment = NSTextAlignmentRight;
    locationLbl.font = [UIFont systemFontOfSize:10.0];
    locationLbl.backgroundColor = [UIColor clearColor];
    locationLbl.textColor = [UIColor whiteColor];
    locationLbl.text = @"深圳";
    [headImgView addSubview:locationLbl];
}
-(void)clickOnLoginBtn{
    LoginVC* loginVc = [[LoginVC alloc] init];
    [self.navigationController pushViewController:loginVc animated:TRUE];
}

-(void) addFocusImage{
    //循环图片
    int length = 4;
    NSMutableArray *tempArray = [NSMutableArray array];
    for (int i = 0 ; i < length; i++)
    {
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"title%d",i],@"title" ,nil];
        [tempArray addObject:dict];
    }
    
    NSMutableArray *itemArray = [NSMutableArray arrayWithCapacity:length+2];
    if (length > 1)
    {
        NSDictionary *dict = [tempArray objectAtIndex:length-1];
        SGFocusImageItem *item = [[SGFocusImageItem alloc] initWithDict:dict tag:-1];
        [itemArray addObject:item];
    }
    for (int i = 0; i < length; i++)
    {
        NSDictionary *dict = [tempArray objectAtIndex:i];
        SGFocusImageItem *item = [[SGFocusImageItem alloc] initWithDict:dict tag:i];
        [itemArray addObject:item];
        
    }
    //添加第一张图 用于循环
    if (length >1)
    {
        NSDictionary *dict = [tempArray objectAtIndex:0];
        SGFocusImageItem *item = [[SGFocusImageItem alloc] initWithDict:dict tag:length];
        [itemArray addObject:item];
    }
    SGFocusImageFrame *bannerView = [[SGFocusImageFrame alloc] initWithFrame:CGRectMake(0, TITLE_HEIGHT , DEVICE_WIDTH, 150) delegate:self imageItems:itemArray isAuto:NO];
    [bannerView scrollToIndex:2];
    [self.view addSubview:bannerView];
}

-(void)addProductRecommendation{
    UIImageView* logoView = [[UIImageView alloc]initWithFrame:CGRectMake(3, 205, 50, 50) ];
    [logoView setImage:[[UIImage  imageNamed:@"btn_checkbuttondown.png"]resizedImageToSize:CGSizeMake(50, 50)]];
    [self.view addSubview:logoView];

    UILabel * prdNameLbl = [[UILabel alloc] initWithFrame:CGRectMake(47, 220, 180, 20)];
    prdNameLbl.textAlignment = NSTextAlignmentLeft;
    prdNameLbl.font = [UIFont systemFontOfSize:12.0];
    prdNameLbl.backgroundColor = [UIColor clearColor];
    prdNameLbl.textColor = [UIColor blackColor];
    prdNameLbl.text = @"四川信托-融石新源燕府";
    [self.view addSubview:prdNameLbl];
    
    
    UILabel * prdYieldLbl = [[UILabel alloc] initWithFrame:CGRectMake(240, 220, 80, 20)];
    prdYieldLbl.textAlignment = NSTextAlignmentLeft;
    prdYieldLbl.font = [UIFont systemFontOfSize:12.0];
    prdYieldLbl.backgroundColor = [UIColor clearColor];
    prdYieldLbl.textColor = [UIColor blackColor];
    prdYieldLbl.text = @"收益率：12%";
    [self.view addSubview:prdYieldLbl];
    
    UILabel * prdYield2Lbl = [[UILabel alloc] initWithFrame:CGRectMake(47, 262, 100, 20)];
    prdYield2Lbl.textAlignment = NSTextAlignmentLeft;
    prdYield2Lbl.font = [UIFont systemFontOfSize:12.0];
    prdYield2Lbl.backgroundColor = [UIColor clearColor];
    prdYield2Lbl.textColor = [UIColor blackColor];
    prdYield2Lbl.text = @"每300万到期收益";
    [self.view addSubview:prdYield2Lbl];
    
    UILabel * prdYield3Lbl = [[UILabel alloc] initWithFrame:CGRectMake(47, 290, 100, 30)];
    prdYield3Lbl.textAlignment = NSTextAlignmentLeft;
    prdYield3Lbl.font = [UIFont systemFontOfSize:32.0];
    prdYield3Lbl.backgroundColor = [UIColor clearColor];
    prdYield3Lbl.textColor = [UIColor redColor];
    prdYield3Lbl.text = @"30.9万";
    [self.view addSubview:prdYield3Lbl];
    
    int atrrX=220;
    
    UILabel * prdAttr1Lbl = [[UILabel alloc] initWithFrame:CGRectMake(atrrX, 260, 40, 15)];
    prdAttr1Lbl.textAlignment = NSTextAlignmentRight;
    prdAttr1Lbl.font = [UIFont systemFontOfSize:12.0];
    prdAttr1Lbl.backgroundColor = [UIColor clearColor];
    prdAttr1Lbl.textColor = [UIColor blackColor];
    prdAttr1Lbl.text = @"风险";
    [self.view addSubview:prdAttr1Lbl];
    
    UILabel * prdAttr2Lbl = [[UILabel alloc] initWithFrame:CGRectMake(atrrX, 285, 40, 15)];
    prdAttr2Lbl.textAlignment = NSTextAlignmentRight;
    prdAttr2Lbl.font = [UIFont systemFontOfSize:12.0];
    prdAttr2Lbl.backgroundColor = [UIColor clearColor];
    prdAttr2Lbl.textColor = [UIColor blackColor];
    prdAttr2Lbl.text = @"担保";
    [self.view addSubview:prdAttr2Lbl];
    
    UILabel * prdAttr3Lbl = [[UILabel alloc] initWithFrame:CGRectMake(atrrX, 310, 40, 15)];
    prdAttr3Lbl.textAlignment = NSTextAlignmentRight;
    prdAttr3Lbl.font = [UIFont systemFontOfSize:12.0];
    prdAttr3Lbl.backgroundColor = [UIColor clearColor];
    prdAttr3Lbl.textColor = [UIColor blackColor];
    prdAttr3Lbl.text = @"抵押";
    [self.view addSubview:prdAttr3Lbl];
    
    UILabel * prdAttr4Lbl = [[UILabel alloc] initWithFrame:CGRectMake(atrrX, 335, 40, 15)];
    prdAttr4Lbl.textAlignment = NSTextAlignmentRight;
    prdAttr4Lbl.font = [UIFont systemFontOfSize:12.0];
    prdAttr4Lbl.backgroundColor = [UIColor clearColor];
    prdAttr4Lbl.textColor = [UIColor blackColor];
    prdAttr4Lbl.text = @"资金监";
    [self.view addSubview:prdAttr4Lbl];
    
    int attrImgX = 250;
    int attrValX = 270;
    UIColor* textColor = [UIColor blackColor];
    
    UIImageView* attrImg1View = [[UIImageView alloc]initWithFrame:CGRectMake(attrImgX, 242, 50, 50) ];
    [attrImg1View setImage:[[UIImage  imageNamed:@"btn_checkbuttondown.png"]resizedImageToSize:CGSizeMake(50, 50)]];
    [self.view addSubview:attrImg1View];
    
    UILabel * attrVal1View = [[UILabel alloc] initWithFrame:CGRectMake(attrValX, 260, 15, 15)];
    attrVal1View.textAlignment = NSTextAlignmentLeft;
    attrVal1View.font = [UIFont systemFontOfSize:12.0];
    attrVal1View.backgroundColor = [UIColor clearColor];
    attrVal1View.textColor = textColor;
    attrVal1View.text = @"低";
    [self.view addSubview:attrVal1View];
    
    UIImageView* attrImg2View = [[UIImageView alloc]initWithFrame:CGRectMake(attrImgX, 267, 50, 50) ];
    [attrImg2View setImage:[[UIImage  imageNamed:@"btn_checkbuttondown.png"]resizedImageToSize:CGSizeMake(50, 50)]];
    [self.view addSubview:attrImg2View];
    
    UILabel * attrVal2View = [[UILabel alloc] initWithFrame:CGRectMake(attrValX, 285, 15, 15)];
    attrVal2View.textAlignment = NSTextAlignmentLeft;
    attrVal2View.font = [UIFont systemFontOfSize:12.0];
    attrVal2View.backgroundColor = [UIColor clearColor];
    attrVal2View.textColor = textColor;
    attrVal2View.text = @"强";
    [self.view addSubview:attrVal2View];
    
    UIImageView* attrImg3View = [[UIImageView alloc]initWithFrame:CGRectMake(attrImgX, 292, 50, 50) ];
    [attrImg3View setImage:[[UIImage  imageNamed:@"btn_checkbuttondown.png"]resizedImageToSize:CGSizeMake(50, 50)]];
    [self.view addSubview:attrImg3View];
    
    UILabel * attrVal3View = [[UILabel alloc] initWithFrame:CGRectMake(attrValX, 310, 15, 15)];
    attrVal3View.textAlignment = NSTextAlignmentLeft;
    attrVal3View.font = [UIFont systemFontOfSize:12.0];
    attrVal3View.backgroundColor = [UIColor clearColor];
    attrVal3View.textColor = textColor;
    attrVal3View.text = @"足";
    [self.view addSubview:attrVal3View];
    
    UIImageView* attrImg4View = [[UIImageView alloc]initWithFrame:CGRectMake(attrImgX, 317, 50, 50) ];
    [attrImg4View setImage:[[UIImage  imageNamed:@"btn_checkbuttondown.png"]resizedImageToSize:CGSizeMake(50, 50)]];
    [self.view addSubview:attrImg4View];
    
    UILabel * attrVal4View = [[UILabel alloc] initWithFrame:CGRectMake(attrValX, 335, 15, 15)];
    attrVal4View.textAlignment = NSTextAlignmentLeft;
    attrVal4View.font = [UIFont systemFontOfSize:12.0];
    attrVal4View.backgroundColor = [UIColor clearColor];
    attrVal4View.textColor = textColor;
    attrVal4View.text = @"管";
    [self.view addSubview:attrVal4View];
    
    //收益
    
    UILabel * profitLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 340, 100, 15)];
    profitLabel.textAlignment = NSTextAlignmentLeft;
    profitLabel.font = [UIFont systemFontOfSize:12.0];
    profitLabel.backgroundColor = [UIColor clearColor];
    profitLabel.textColor = textColor;
    profitLabel.text = @"产品收益：30万";
    [self.view addSubview:profitLabel];
    UILabel * profit2Label = [[UILabel alloc] initWithFrame:CGRectMake(40, 358, 100, 15)];
    profit2Label.textAlignment = NSTextAlignmentLeft;
    profit2Label.font = [UIFont systemFontOfSize:12.0];
    profit2Label.backgroundColor = [UIColor clearColor];
    profit2Label.textColor = textColor;
    profit2Label.text = @"赠送红包：0.9万";
    [self.view addSubview:profit2Label];
    
    //关注人数
    
    UILabel * attentionLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 340, 50, 15)];
    attentionLabel.textAlignment = NSTextAlignmentLeft;
    attentionLabel.font = [UIFont systemFontOfSize:12.0];
    attentionLabel.backgroundColor = [UIColor clearColor];
    attentionLabel.textColor = textColor;
    attentionLabel.text = @"10000人";
    [self.view addSubview:attentionLabel];
    UILabel * attention2Label = [[UILabel alloc] initWithFrame:CGRectMake(150, 358, 60, 15)];
    attention2Label.textAlignment = NSTextAlignmentLeft;
    attention2Label.font = [UIFont systemFontOfSize:12.0];
    attention2Label.backgroundColor = [UIColor clearColor];
    attention2Label.textColor = textColor;
    attention2Label.text = @"咨询过产品";
    [self.view addSubview:attention2Label];
    
    //起价
    UILabel * minInvestLabel = [[UILabel alloc] initWithFrame:CGRectMake(220, 358, 100, 15)];
    minInvestLabel.textAlignment = NSTextAlignmentLeft;
    minInvestLabel.font = [UIFont systemFontOfSize:12.0];
    minInvestLabel.backgroundColor = [UIColor clearColor];
    minInvestLabel.textColor = textColor;
    minInvestLabel.text = @"12个月100万起";
    [self.view addSubview:minInvestLabel];
    
    //button
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(20, DEVICE_HEIGHT - BOTTOM_HEIGHT - 40, 280, 30)];
    [button setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateSelected];
    [button setTitle:@"预约" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:20.0]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:button];
    

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addHeader];
    
    [self addFocusImage];
    [self addProductRecommendation];
    
    

    

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
