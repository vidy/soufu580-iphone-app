//
//  StrategyDetailVC.m
//  Soufu580
//
//  Created by  apple on 01-1-1.
//  Copyright (c) 2001年 Soufu580. All rights reserved.
//

#import "StrategyDetailVC.h"
#import "defines.h"

NSInteger kMarkTypeGood = 0;
NSInteger kMarkTypeBad = 1;

@interface StrategyDetailVC (){
    
    StrategyAbs * abs;
}

-(void)addStrategyDetail;

@end

@implementation StrategyDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithStrategyAbs:(StrategyAbs*)strabs{
    
    self = [super init];
    if (self) {
        abs = strabs;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addTopViewWithTitle:[NSString stringWithFormat:@"%@",abs.title] leftButtonTitle:@"返回" rightButtonTitle:nil];
    
    [self addStrategyDetail];
}

-(void)addStrategyDetail{
    
    UITextView * txtView = [[UITextView alloc] initWithFrame:CGRectMake(5, TITLE_HEIGHT+5, DEVICE_WIDTH-2*5,DEVICE_HEIGHT-TITLE_HEIGHT-BOTTOM_HEIGHT-90)];
    txtView.editable = FALSE;
    txtView.backgroundColor = [UIColor clearColor];
    txtView.font = [UIFont systemFontOfSize:18.0];
    txtView.text = abs.abs;
    
    [self.view addSubview:txtView];
    
    // add mark good button
    UIButton *goodBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, TITLE_HEIGHT+txtView.frame.size.height+15, 80.0, 28.0)];
    [goodBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeidefault1.png"] forState:UIControlStateNormal];
    [goodBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeiselected2.png"] forState:UIControlStateSelected];
    [goodBut setTitle:@"对我有用" forState:UIControlStateNormal];
    [goodBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [goodBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [goodBut addTarget:self action:@selector(clickOnMarkBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:goodBut];
    goodBut.tag = kMarkTypeGood;
    
    [self.view addSubview:goodBut];
    
    // add mark bad button
    UIButton *badBut = [[UIButton alloc] initWithFrame:CGRectMake(105.0, TITLE_HEIGHT+txtView.frame.size.height+15, 80.0, 28.0)];
    [badBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeidefault1.png"] forState:UIControlStateNormal];
    [badBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeiselected2.png"] forState:UIControlStateSelected];
    [badBut setTitle:@"对我没用" forState:UIControlStateNormal];
    [badBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [badBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [badBut addTarget:self action:@selector(clickOnMarkBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:badBut];
    badBut.tag = kMarkTypeBad;
    
    // add next button
    UIButton *nxtBut = [[UIButton alloc] initWithFrame:CGRectMake(225.0, DEVICE_HEIGHT-BOTTOM_HEIGHT-35, 80.0, 28.0)];
    [nxtBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeidefault1.png"] forState:UIControlStateNormal];
    [nxtBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeiselected2.png"] forState:UIControlStateSelected];
    [nxtBut setTitle:@"下一个" forState:UIControlStateNormal];
    [nxtBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [nxtBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nxtBut addTarget:self action:@selector(clickOnNextBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nxtBut];
}

#pragma mark - 按钮事件 
-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)clickOnMarkBut:(id)btn{
    NSInteger btnTag = [(UIButton*)btn tag];
    if (btnTag == kMarkTypeBad) {
        //bad mark
    }else{
        //good mark
    }
}

-(void)clickOnNextBut:(id)btn{
    //next strategy
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
