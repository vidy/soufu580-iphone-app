//
//  StrategyAbs.h
//  Soufu580
//
//  Created by  apple on 14-4-19.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StrategyAbs : NSObject {
    NSString * _abs;
    NSString * _title;
    NSInteger _stepNum;
    
    NSString * _imgUrl;
    NSString * _detailUrl;
    NSInteger _goodMark;
    NSInteger _badMark;
}

@property (nonatomic, retain) NSString * abs;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * imgUrl;
@property (nonatomic, retain) NSString * detailUrl;
@property (nonatomic, readwrite) NSInteger stepNum;
@property (nonatomic, readwrite) NSInteger goodMark;
@property (nonatomic, readwrite) NSInteger badMark;

@end