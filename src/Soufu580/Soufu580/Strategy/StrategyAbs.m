//
//  StrategyAbs.m
//  Soufu580
//
//  Created by  apple on 14-4-19.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "StrategyAbs.h"

@implementation StrategyAbs

@synthesize abs = _abs;
@synthesize title = _title;
@synthesize imgUrl = _imgUrl;
@synthesize detailUrl = _detailUrl;
@synthesize stepNum = _stepNum;
@synthesize goodMark = _goodMark;
@synthesize badMark = _badMark;

@end