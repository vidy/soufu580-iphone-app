//
//  StrategyListCell.h
//  Soufu580
//
//  Created by  apple on 14-4-19.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StrategyAbs.h"

@interface StrategyListCell : UITableViewCell

- (id)initWithStrategyAbs:(StrategyAbs*)abs;
- (id)updateStrategyAbs:(StrategyAbs*)abs;

@end


extern NSString * kStrategyListCellIdentifier;
extern NSInteger kStrategyListCellHeight;