//
//  StrategyListVC.h
//  Soufu580
//
//  Created by  apple on 14-4-19.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "BaseVC.h"
#import "StrategyAbs.h"

@interface StrategyListVC : BaseVC

- (id)initWithStrategyAbs:(StrategyAbs*)abs;

@end
