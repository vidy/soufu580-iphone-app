//
//  StrategyListCell.m
//  Soufu580
//
//  Created by  apple on 14-4-19.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "StrategyListCell.h"
#import "UIImage+Resize.h"

NSString * kStrategyListCellIdentifier = @"StrategyListCell";
NSInteger kStrategyListCellHeight = 80;

@interface StrategyListCell () {
    StrategyAbs * _abs;
    
    UIImageView * logoView;
    UILabel * titleLb;
    UILabel * absLb;
    UILabel * goodLb;
    UILabel * badLb;
}

@property (nonatomic, retain) StrategyAbs * abs;

- (void)flushCellView;

@end


@implementation StrategyListCell

@synthesize abs = _abs;

- (id)initWithStrategyAbs:(StrategyAbs *)abs{
    
    self.abs = abs;
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kStrategyListCellIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        
        
        //add logo image
        logoView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, kStrategyListCellHeight-20, kStrategyListCellHeight-20)];
        [logoView setImage:[[UIImage imageNamed:@"wantDriverSel.png"] resizedImageToFitInSize:CGSizeMake(kStrategyListCellHeight-20, kStrategyListCellHeight-20) scaleIfSmaller:TRUE]];
        logoView.tag = 1001;
        [self addSubview:logoView];
        
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(kStrategyListCellHeight, 3, 200, 20)];
        titleLb.backgroundColor = [UIColor clearColor];
        titleLb.font = [UIFont boldSystemFontOfSize:14.0];
        titleLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        titleLb.tag = 1002;
        [self addSubview:titleLb];
        
        absLb = [[UILabel alloc] initWithFrame:CGRectMake(kStrategyListCellHeight, 22, 200, kStrategyListCellHeight-24)];
        absLb.backgroundColor = [UIColor clearColor];
        absLb.font = [UIFont systemFontOfSize:12.0];
        absLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        absLb.numberOfLines = 0;
        absLb.tag = 1003;
        [self addSubview:absLb];
        
        //add good mark image
        UIImageView * goodView = [[UIImageView alloc] initWithFrame:CGRectMake(270, 5, 30, 30)];
        [goodView setImage:[[UIImage imageNamed:@"btn_stardown.png"] resizedImageToFitInSize:CGSizeMake(20, 20) scaleIfSmaller:TRUE]];
        goodView.tag = 1004;
        [self addSubview:goodView];
        
        goodLb = [[UILabel alloc] initWithFrame:CGRectMake(295, 10, 20, 20)];
        goodLb.backgroundColor = [UIColor clearColor];
        goodLb.font = [UIFont boldSystemFontOfSize:14.0];
        goodLb.textAlignment = NSTextAlignmentCenter;
        goodLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        goodLb.tag = 1005;
        [self addSubview:goodLb];
        
        //add bad mark image
        UIImageView * badView = [[UIImageView alloc] initWithFrame:CGRectMake(270, 35, 30, 30)];
        [badView setImage:[[UIImage imageNamed:@"btn_starup.png"] resizedImageToFitInSize:CGSizeMake(20, 20) scaleIfSmaller:TRUE]];
        badView.tag = 1006;
        [self addSubview:badView];
        
        badLb = [[UILabel alloc] initWithFrame:CGRectMake(295, 40, 20, 20)];
        badLb.backgroundColor = [UIColor clearColor];
        badLb.font = [UIFont boldSystemFontOfSize:14.0];
        badLb.textAlignment = NSTextAlignmentCenter;
        badLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        badLb.tag = 1007;
        [self addSubview:badLb];
    }
    
    [self flushCellView];
    
    return self;
}

- (id)updateStrategyAbs:(StrategyAbs*)abs{
    self.abs = abs;
    
    [self flushCellView];
    
    return self;
}

- (void)flushCellView{
    
    /*for (UIView * subView in self.subviews) {
        if (subView.tag>100) {
            [subView removeFromSuperview];
        }
    }*/
    
    //add textlabel
    
    // set the logo image
    
    titleLb.text = [NSString stringWithFormat:@"%@",self.abs.title];
    
    absLb.text = [NSString stringWithFormat:@"%@",self.abs.abs];

    goodLb.text = [NSString stringWithFormat:@"%d",self.abs.goodMark];

    badLb.text = [NSString stringWithFormat:@"%d",self.abs.badMark];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
