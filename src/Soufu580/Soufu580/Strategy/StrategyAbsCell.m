//
//  StrategyAbsCell.m
//  Soufu580
//
//  Created by  apple on 14-4-19.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "StrategyAbsCell.h"
#import "UIImage+Resize.h"

NSString * kStrategyAbsCellIdentifier = @"StrategyAbsCell";
NSInteger kStrategyAbsCellHeight = 70;


@interface StrategyAbsCell () {
    StrategyAbs * _abs;
    
    UIImageView * logoView;
    UILabel * titleLb;
    UILabel * absLb;
    UILabel * stepLb;
}

@property (nonatomic, retain) StrategyAbs * abs;

- (void)flushCellView;

@end

@implementation StrategyAbsCell

@synthesize abs = _abs;

- (id)initWithStrategyAbs:(StrategyAbs *)abs{
    
    self.abs = abs;
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kStrategyAbsCellIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
        
        //add textlabel
        UIImageView * bgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 240, kStrategyAbsCellHeight-15)];
        [bgView setImage:[[UIImage imageNamed:@"btn_xiadandefault1.png"] resizedImageToFitInSize:CGSizeMake(240, kStrategyAbsCellHeight-15) scaleIfSmaller:TRUE]];
        bgView.tag = 1001;
        [self addSubview:bgView];
        
        titleLb = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, 240, 20)];
        titleLb.backgroundColor = [UIColor clearColor];
        titleLb.font = [UIFont boldSystemFontOfSize:14.0];
        titleLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        titleLb.tag = 1001;
        [self addSubview:titleLb];
        
        absLb = [[UILabel alloc] initWithFrame:CGRectMake(20, 30, 240, 30)];
        absLb.backgroundColor = [UIColor clearColor];
        absLb.font = [UIFont systemFontOfSize:12.0];
        absLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        absLb.numberOfLines = 0;
        absLb.tag = 1002;
        [self addSubview:absLb];
        
        //add step number image
        UIImageView * stepView = [[UIImageView alloc] initWithFrame:CGRectMake(260, 0, kStrategyAbsCellHeight, kStrategyAbsCellHeight)];
        [stepView setImage:[[UIImage imageNamed:@"btn_checkbuttondown.png"] resizedImageToFitInSize:CGSizeMake(kStrategyAbsCellHeight, kStrategyAbsCellHeight) scaleIfSmaller:TRUE]];
        stepView.tag = 1003;
        [self addSubview:stepView];
        
        stepLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, kStrategyAbsCellHeight, kStrategyAbsCellHeight)];
        stepLb.backgroundColor = [UIColor clearColor];
        stepLb.font = [UIFont boldSystemFontOfSize:14.0];
        stepLb.textAlignment = NSTextAlignmentCenter;
        stepLb.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
        [stepView addSubview:stepLb];
    }
    
    [self flushCellView];
    
    return self;
}

- (id)updateStrategyAbs:(StrategyAbs*)abs{
    self.abs = abs;
    
    [self flushCellView];
    
    return self;
}

- (void)flushCellView{
    
    //add textlabel
    titleLb.text = [NSString stringWithFormat:@"Step %d       %@",self.abs.stepNum,self.abs.title];
    
    absLb.text = [NSString stringWithFormat:@"%@",self.abs.abs];
    
    stepLb.text = [NSString stringWithFormat:@"%d",self.abs.stepNum];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
