//
//  StrategyVC.m
//  Soufu580
//
//  Created by  apple on 14-4-18.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "StrategyVC.h"
#import "UIImage+Resize.h"
#import "StrategyAbsCell.h"
#import "StrategyListVC.h"
#import "defines.h"

NSInteger kBottomLabelHeight = 30;
NSInteger kPhoneBtnTag = 100;
NSInteger kWebsiteBtnTag = 101;
NSInteger kMobileBtnTag = 102;
NSInteger kWeixinBtnTag = 103;

@interface StrategyVC () <UITableViewDataSource,UITableViewDelegate>{
    NSMutableDictionary * cardDict;
    NSMutableArray * stepArr;
    UITableView * stepTable;
}

-(void)addStepTable;

@end

@implementation StrategyVC

- (id)init {
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"攻略" image:nil tag:0];
        
        [[self tabBarItem] setSelectedImage:[[UIImage imageNamed:@"jiezhangButSel"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        [[self tabBarItem] setImage:[[UIImage imageNamed:@"jiezhangBut"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        
        
        [[self tabBarItem] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                                   [UIFont systemFontOfSize:12.0], NSFontAttributeName,
                                                   nil] forState:UIControlStateNormal];
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addTopViewWithTitle:@"攻略" leftButtonTitle:nil rightButtonTitle:nil];
    
    // test data
    
    stepArr = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<7; i++) {
        StrategyAbs * abs = [[StrategyAbs alloc] init];
        abs.abs = @"在线咨询理财经理，预约理财产品";
        abs.stepNum = i+1;
        abs.title = @"在线咨询";
        
        [stepArr addObject:abs];
    }
    
    [self addStepTable];
}


-(void)addStepTable{

    //加载常用地址数据列表AddressTable
    stepTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, TITLE_HEIGHT, DEVICE_WIDTH, DEVICE_HEIGHT-TITLE_HEIGHT-BOTTOM_HEIGHT-kBottomLabelHeight-2) style:UITableViewStylePlain];
    stepTable.delegate = self;
    stepTable.dataSource = self;
    stepTable.backgroundColor = [UIColor clearColor];
    stepTable.bounces = NO;
    stepTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:stepTable];
    
    UIButton * phoneBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, DEVICE_HEIGHT-BOTTOM_HEIGHT-kBottomLabelHeight, DEVICE_WIDTH/2, kBottomLabelHeight)];
    [phoneBtn setTitle:@"投资热线:400-8250-580" forState:UIControlStateNormal];
    [phoneBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [phoneBtn setTitleColor:[UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0] forState:UIControlStateNormal];
    [phoneBtn addTarget:self action:@selector(clickOnBut:) forControlEvents:UIControlEventTouchUpInside];
    phoneBtn.tag = kPhoneBtnTag;
    [self.view addSubview:phoneBtn];
    
    UIButton * webBtn = [[UIButton alloc] initWithFrame:CGRectMake(160, DEVICE_HEIGHT-BOTTOM_HEIGHT-kBottomLabelHeight, 50, kBottomLabelHeight)];
    [webBtn setTitle:@"网页版  |" forState:UIControlStateNormal];
    [webBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [webBtn setTitleColor:[UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0] forState:UIControlStateNormal];
    [webBtn addTarget:self action:@selector(clickOnBut:) forControlEvents:UIControlEventTouchUpInside];
    webBtn.tag = kWebsiteBtnTag;
    [self.view addSubview:webBtn];
    
    UIButton * mobileBtn = [[UIButton alloc] initWithFrame:CGRectMake(210, DEVICE_HEIGHT-BOTTOM_HEIGHT-kBottomLabelHeight, 50, kBottomLabelHeight)];
    [mobileBtn setTitle:@"手机版  |" forState:UIControlStateNormal];
    [mobileBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [mobileBtn setTitleColor:[UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0] forState:UIControlStateNormal];
    [mobileBtn addTarget:self action:@selector(clickOnBut:) forControlEvents:UIControlEventTouchUpInside];
    mobileBtn.tag = kMobileBtnTag;
    [self.view addSubview:mobileBtn];
    
    UIButton * weixinBtn = [[UIButton alloc] initWithFrame:CGRectMake(260, DEVICE_HEIGHT-BOTTOM_HEIGHT-kBottomLabelHeight, 60, kBottomLabelHeight)];
    [weixinBtn setTitle:@"关注微信" forState:UIControlStateNormal];
    [weixinBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    [weixinBtn setTitleColor:[UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0] forState:UIControlStateNormal];
    [weixinBtn addTarget:self action:@selector(clickOnBut:) forControlEvents:UIControlEventTouchUpInside];
    weixinBtn.tag = kWeixinBtnTag;
    [self.view addSubview:weixinBtn];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - 按钮事件处理
- (void)clickOnBut:(id)button{
    
    UIButton * btn  = (UIButton*)button;
    NSLog(@"button tag is %d",btn.tag);
    
    if (btn.tag == kPhoneBtnTag) {
        NSString * realPhone = @"4008250580";
        NSString * phone = [NSString stringWithFormat:@"tel://%@",realPhone];
        UIWebView * callWebview =[[UIWebView alloc] init];
        NSURL *telURL =[NSURL URLWithString:phone];
        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
        [self.view addSubview:callWebview];
    }else if (btn.tag == kMobileBtnTag){
        
    }else if (btn.tag == kWebsiteBtnTag){
        NSString * url = @"http://www.sofu580.com/";
        UIWebView * callWebview =[[UIWebView alloc] init];
        NSURL *telURL =[NSURL URLWithString:url];
        [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
        [self.view addSubview:callWebview];
    }else if (btn.tag == kWeixinBtnTag){
        
    }
}

#pragma mark - UITableView的协议处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [stepArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kStrategyAbsCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    StrategyAbs * strategy = [stepArr objectAtIndex:indexPath.row];
    
    StrategyListVC * vc = [[StrategyListVC alloc] initWithStrategyAbs:strategy];
    [self.navigationController pushViewController:vc animated:TRUE];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    StrategyAbs * strategy = [stepArr objectAtIndex:indexPath.row];
    
    StrategyAbsCell * cell = [tableView dequeueReusableCellWithIdentifier:kStrategyAbsCellIdentifier];
    
    if (cell==nil) {
        cell = [[StrategyAbsCell alloc] initWithStrategyAbs:strategy];
        cell.userInteractionEnabled = YES;
    }else{
        [cell updateStrategyAbs:strategy];
    }
    
    
    return cell;
}

@end
