//
//  StrategyListVC.m
//  Soufu580
//
//  Created by  apple on 14-4-19.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "StrategyListVC.h"
#import "StrategyListCell.h"
#import "StrategyDetailVC.h"
#import "defines.h"

@interface StrategyListVC ()<UITableViewDataSource,UITableViewDelegate>{
    StrategyAbs * abs;
    
    NSMutableDictionary * cardDict;
    NSMutableArray * strategyArr;
    UITableView * strategyTable;
}

-(void)addStrategyTable;
@end

@implementation StrategyListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithStrategyAbs:(StrategyAbs*)strabs{
    
    self = [super init];
    if (self) {
        abs = strabs;
        
        strategyArr = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < 9; i++) {
            StrategyAbs * absObj = [[StrategyAbs alloc] init];
            absObj.abs = @"本项目设计单位为北京中广国际，资质甲级；施工单位为中国建筑，资质特级；监理单位为于南国开建设监理咨询，资质为甲级资质。项目作为保山首席法式风情园林社区，品质有保障，目前本项目进展顺利，五证齐全；幵将通过多渠道选择实力雄厚、经验丰富的易居中国作为总代理公司进行销售，以保证适时的销售时机。本项目设计单位为北京中广国际，资质甲级；施工单位为中国建筑，资质特级；监理单位为于南国开建设监理咨询，资质为甲级资质。项目作为保山首席法式风情园林社区，品质有保障，目前本项目进展顺利，五证齐全；幵将通过多渠道选择实力雄厚、经验丰富的易居中国作为总代理公司进行销售，以保证适时的销售时机。本项目设计单位为北京中广国际，资质甲级；施工单位为中国建筑，资质特级；监理单位为于南国开建设监理咨询，资质为甲级资质。项目作为保山首席法式风情园林社区，品质有保障，目前本项目进展顺利，五证齐全；幵将通过多渠道选择实力雄厚、经验丰富的易居中国作为总代理公司进行销售，以保证适时的销售时机。";
            absObj.stepNum = i+1;
            absObj.title = @"在线咨询";
            absObj.goodMark = i+50;
            absObj.badMark = i+1;
            
            [strategyArr addObject:absObj];
        }
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addTopViewWithTitle:[NSString stringWithFormat:@"%@",abs.title] leftButtonTitle:@"返回" rightButtonTitle:nil];
    
    [self addStrategyTable];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)addStrategyTable{
    
    //加载常用地址数据列表AddressTable
    strategyTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, TITLE_HEIGHT, DEVICE_WIDTH, DEVICE_HEIGHT-TITLE_HEIGHT-BOTTOM_HEIGHT) style:UITableViewStylePlain];
    strategyTable.delegate = self;
    strategyTable.dataSource = self;
    strategyTable.backgroundColor = [UIColor clearColor];
    strategyTable.bounces = NO;
    [self.view addSubview:strategyTable];
}


#pragma mark - UITableView的协议处理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [strategyArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kStrategyListCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    StrategyAbs * strategy = [strategyArr objectAtIndex:indexPath.row];
    
    StrategyDetailVC * vc = [[StrategyDetailVC alloc] initWithStrategyAbs:strategy];
    [self.navigationController pushViewController:vc animated:TRUE];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    StrategyAbs * strategy = [strategyArr objectAtIndex:indexPath.row];
    
    StrategyListCell * cell = [tableView dequeueReusableCellWithIdentifier:kStrategyListCellIdentifier];
    
    if (cell==nil) {
        cell = [[StrategyListCell alloc] initWithStrategyAbs:strategy];
        cell.userInteractionEnabled = YES;
    }else{
        [cell updateStrategyAbs:strategy];
    }
    
    
    return cell;
}

@end
