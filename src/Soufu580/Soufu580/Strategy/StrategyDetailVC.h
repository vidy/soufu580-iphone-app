//
//  StrategyDetailVC.h
//  Soufu580
//
//  Created by  apple on 01-1-1.
//  Copyright (c) 2001年 Soufu580. All rights reserved.
//

#import "BaseVC.h"
#import "StrategyAbs.h"

@interface StrategyDetailVC : BaseVC

- (id)initWithStrategyAbs:(StrategyAbs*)abs;

@end
