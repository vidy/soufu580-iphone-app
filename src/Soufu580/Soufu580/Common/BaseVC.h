//
//  BaseVC.h
//  Soufu580
//
//  Created by  apple on 14-4-18.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController{
    UIButton * _rightButton;
    UIButton * _leftButton;
}

@property (nonatomic, retain) UIButton * rightButton;
@property (nonatomic, retain) UIButton * leftButton;

//title bar
-(void)addTopViewWithTitle:(NSString *)title leftButtonTitle:(NSString*)left rightButtonTitle:(NSString*)right;

//button event
-(void)clickOnRightBut;
-(void)clickOnLeftBut;

//progress view
- (void)dismiss;
- (void)dismissSuccessMsg:(NSString*)msg;
- (void)dismissErrorMsg:(NSString*)msg;
@end


