//
//  RegisterVC.m
//  Soufu580
//
//  Created by vidy on 14-4-26.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "RegisterVC.h"
#import "defines.h"

@interface RegisterVC ()

@end

@implementation RegisterVC

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTopViewWithTitle:@"本机号码注册" leftButtonTitle:@"返回" rightButtonTitle:nil];
    
    UILabel * userLbl = [[UILabel alloc] initWithFrame:CGRectMake(30,TITLE_HEIGHT+20 , 50, 20)];
    userLbl.backgroundColor = [UIColor clearColor];
    userLbl.font = [UIFont boldSystemFontOfSize:14.0];
    userLbl.textAlignment=NSTextAlignmentRight;
    userLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    userLbl.text = @"姓名:";
    [self.view addSubview:userLbl];
    
    UITextField * userText = [[UITextField alloc] initWithFrame:CGRectMake(85, TITLE_HEIGHT+20 , 160, 20)];
    userText.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:userText];

    
    
    UILabel * mailLbl = [[UILabel alloc] initWithFrame:CGRectMake(30,TITLE_HEIGHT+50 , 50, 20)];
    mailLbl.backgroundColor = [UIColor clearColor];
    mailLbl.font = [UIFont boldSystemFontOfSize:14.0];
    mailLbl.textAlignment=NSTextAlignmentRight;
    mailLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    mailLbl.text = @"邮箱:";
    [self.view addSubview:mailLbl];
    
    UITextField * mailText = [[UITextField alloc] initWithFrame:CGRectMake(85, TITLE_HEIGHT+50 , 160, 20)];
    mailText.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:mailText];
    
    UILabel * pwdLbl = [[UILabel alloc] initWithFrame:CGRectMake(30,TITLE_HEIGHT+80 , 50, 20)];
    pwdLbl.backgroundColor = [UIColor clearColor];
    pwdLbl.font = [UIFont boldSystemFontOfSize:14.0];
    pwdLbl.textAlignment=NSTextAlignmentRight;
    pwdLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    pwdLbl.text = @"密码:";
    [self.view addSubview:pwdLbl];
    
    UITextField * pwdText = [[UITextField alloc] initWithFrame:CGRectMake(85, TITLE_HEIGHT+80 , 160, 20)];
    pwdText.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pwdText];
    

    
    
    UIButton *lgnBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, TITLE_HEIGHT+110, 220, 30)];
    [lgnBtn setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateNormal];
    [lgnBtn setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateSelected];
    [lgnBtn setTitle:@"注册" forState:UIControlStateNormal];
    [lgnBtn.titleLabel setFont:[UIFont systemFontOfSize:20.0]];
    [lgnBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:lgnBtn];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}

@end
