//
//  BaseVC.m
//  Soufu580
//
//  Created by  apple on 14-4-18.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "BaseVC.h"
#import "SVProgressHUD.h"
#import "defines.h"

@interface BaseVC ()

@end

@implementation BaseVC

@synthesize rightButton = _rightButton;
@synthesize leftButton = _leftButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - 添加顶部条栏和文字
-(void)addTopViewWithTitle:(NSString *)title leftButtonTitle:(NSString*)left rightButtonTitle:(NSString*)right{
    //背景图
    UIImageView * bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DEVICE_WIDTH, DEVICE_HEIGHT)];
    
    [bgImgView setImage:[UIImage imageNamed:@"bg_640x960.png"]];
    [self.view addSubview:bgImgView];
    
    //顶部条栏,原高度为47
    UIImageView * topBarView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, DEVICE_WIDTH, TITLE_HEIGHT)];
    topBarView.image = [UIImage imageNamed:@"hengtiaolang_tltle.png"];
    [self.view addSubview:topBarView];
    
    //顶部文字
    UILabel * topLbl = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, DEVICE_WIDTH, TITLE_HEIGHT)];
    topLbl.textAlignment = NSTextAlignmentCenter;
    topLbl.font = [UIFont systemFontOfSize:22.0];
    topLbl.backgroundColor = [UIColor clearColor];
    topLbl.textColor = [UIColor whiteColor];
    topLbl.text = title;
    [self.view addSubview:topLbl];
    
    //左边按钮
    if (left) {
        UIButton *standardBut = [[UIButton alloc] initWithFrame:CGRectMake(5.0, 16.0, 64.0, 28.0)];
        [standardBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeidefault1.png"] forState:UIControlStateNormal];
        [standardBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeiselected2.png"] forState:UIControlStateSelected];
        [standardBut setTitle:left forState:UIControlStateNormal];
        [standardBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [standardBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [standardBut addTarget:self action:@selector(clickOnLeftBut) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:standardBut];
        self.leftButton = standardBut;
        
    }else{
        self.leftButton = nil;
    }
    
    //右边按钮
    if (right) {
        UIButton * settingBut = [[UIButton alloc] initWithFrame:CGRectMake(268.0, 16.0, 46.0, 28.0)];
        [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeidefault1"] forState:UIControlStateNormal];
        [settingBut setBackgroundImage:[UIImage imageNamed:@"btn_shoufeiselected2"] forState:UIControlStateSelected];
        [settingBut setTitle:right forState:UIControlStateNormal];
        [settingBut.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
        [settingBut setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [settingBut addTarget:self action:@selector(clickOnRightBut) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:settingBut];
        self.rightButton = settingBut;
        
    }else{
        self.rightButton = nil;
    }
}

#pragma mark - 按钮事件
-(void)clickOnRightBut{
    
}

-(void)clickOnLeftBut{
    
}

#pragma mark -
#pragma mark 隐藏提醒框
- (void)dismiss {
	[SVProgressHUD dismiss];
}

- (void)dismissSuccessMsg:(NSString*)msg {
	[SVProgressHUD showSuccessWithStatus:msg];
}

- (void)dismissErrorMsg:(NSString*)msg {
	[SVProgressHUD showErrorWithStatus:msg];
    NSLog(@"dismissErrorMsg: called");
}

@end
