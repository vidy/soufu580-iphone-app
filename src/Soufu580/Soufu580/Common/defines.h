//
//  defines.h
//  Soufu580
//
//  Created by  apple on 14-4-20.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DEVICE_IS_IPHONE5 ([[UIScreen mainScreen] bounds].size.height == 568)

#define DEVICE_HEIGHT (DEVICE_IS_IPHONE5 ? 568 : 480)
#define DEVICE_WIDTH 320

#define TITLE_HEIGHT 55
#define BOTTOM_HEIGHT 49

#define INVALID_HEIGHT (DEVICE_IS_IPHONE5 ? 464 : 376)
