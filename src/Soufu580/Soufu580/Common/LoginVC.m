//
//  LoginVC.m
//  Soufu580
//
//  Created by vidy on 14-4-26.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "LoginVC.h"
#import "defines.h"
#import "RegisterVC.h"

@interface LoginVC ()

@end

@implementation LoginVC

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTopViewWithTitle:@"登录" leftButtonTitle:@"返回" rightButtonTitle:nil];
    
    UILabel * telLbl = [[UILabel alloc] initWithFrame:CGRectMake(30,TITLE_HEIGHT+20 , 50, 20)];
    telLbl.backgroundColor = [UIColor clearColor];
    telLbl.font = [UIFont boldSystemFontOfSize:14.0];
    telLbl.textAlignment=NSTextAlignmentRight;
    telLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    telLbl.text = @"手机号:";
    [self.view addSubview:telLbl];
    
    UITextField * telText = [[UITextField alloc] initWithFrame:CGRectMake(85, TITLE_HEIGHT+20 , 160, 20)];
    telText.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:telText];
    
    UILabel * pwdLbl = [[UILabel alloc] initWithFrame:CGRectMake(30,TITLE_HEIGHT+50 , 50, 20)];
    pwdLbl.backgroundColor = [UIColor clearColor];
    pwdLbl.font = [UIFont boldSystemFontOfSize:14.0];
    pwdLbl.textAlignment=NSTextAlignmentRight;
    pwdLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    pwdLbl.text = @"密码:";
    [self.view addSubview:pwdLbl];
    
    UITextField * pwdText = [[UITextField alloc] initWithFrame:CGRectMake(85, TITLE_HEIGHT+50 , 160, 20)];
    pwdText.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:pwdText];
    
    UIButton *lgnBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, TITLE_HEIGHT+80, 220, 30)];
    [lgnBtn setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateNormal];
    [lgnBtn setBackgroundImage:[UIImage imageNamed:@"btn_xiadandefault1.png"] forState:UIControlStateSelected];
    [lgnBtn setTitle:@"登录" forState:UIControlStateNormal];
    [lgnBtn.titleLabel setFont:[UIFont systemFontOfSize:20.0]];
    [lgnBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:lgnBtn];
    
    UIButton *regBtn = [[UIButton alloc] initWithFrame:CGRectMake(200, TITLE_HEIGHT+120, 50, 20)];
    [regBtn setTitle:@"注册新用户" forState:UIControlStateNormal];
    [regBtn.titleLabel setTextAlignment:NSTextAlignmentRight];
    [regBtn.titleLabel setFont:[UIFont systemFontOfSize:10.0]];
    [regBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [regBtn addTarget:self action:@selector(clickOnRegBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:regBtn];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}
-(void)clickOnRegBtn{
    RegisterVC* regVc = [[RegisterVC alloc]init];
    [self.navigationController pushViewController:regVc animated:TRUE];
}

@end
