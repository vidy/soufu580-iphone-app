//
//  main.m
//  Soufu580
//
//  Created by vidy on 14-4-16.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SoufuAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SoufuAppDelegate class]));
    }
}
