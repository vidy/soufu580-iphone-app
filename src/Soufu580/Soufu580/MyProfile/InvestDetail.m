//
//  InvestDetail.m
//  Soufu580
//
//  Created by vidy on 14-4-23.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "InvestDetail.h"
#import "defines.h"

@interface InvestDetail ()
{
    InvestProduct* product;
}
@end

@implementation InvestDetail

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}
-(id)initWithInvestProduct:(InvestProduct *)prd
{
    self = [super init];
    if(self){
        product = prd;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addTopViewWithTitle:product.orderId leftButtonTitle:@"返回" rightButtonTitle:nil];
    
    UILabel * orderLbl = [[UILabel alloc] initWithFrame:CGRectMake(0,TITLE_HEIGHT , DEVICE_WIDTH, 30)];
    orderLbl.backgroundColor = [UIColor whiteColor];
    orderLbl.font = [UIFont boldSystemFontOfSize:14.0];
    orderLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    orderLbl.text = [NSString stringWithFormat:@"订单详情       %@              %@",product.status,product.date];
    [self.view addSubview:orderLbl];
    
    
    
    UILabel * orderLbl2 = [[UILabel alloc] initWithFrame:CGRectMake(0,TITLE_HEIGHT+31 , DEVICE_WIDTH, 30)];
    orderLbl2.backgroundColor = [UIColor whiteColor];
    orderLbl2.font = [UIFont boldSystemFontOfSize:14.0];
    orderLbl2.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    orderLbl2.text = [NSString stringWithFormat:@"预约产品：%@",product.prdName];
    [self.view addSubview:orderLbl2];
    
    UILabel * orderLbl3 = [[UILabel alloc] initWithFrame:CGRectMake(0,TITLE_HEIGHT+61 , DEVICE_WIDTH, 30)];
    orderLbl3.backgroundColor = [UIColor whiteColor];
    orderLbl3.font = [UIFont boldSystemFontOfSize:14.0];
    orderLbl3.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    orderLbl3.text = [NSString stringWithFormat:@"预约理财经理:%@     经理电话:%@",product.agent,product.phone];
    [self.view addSubview:orderLbl3];
    
    UILabel * orderLbl4 = [[UILabel alloc] initWithFrame:CGRectMake(0,TITLE_HEIGHT+91 , DEVICE_WIDTH, 30)];
    orderLbl4.backgroundColor = [UIColor whiteColor];
    orderLbl4.font = [UIFont boldSystemFontOfSize:14.0];
    orderLbl4.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    orderLbl4.text = [NSString stringWithFormat:@"预约金额:%@",product.amount];
    [self.view addSubview:orderLbl4];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(30, TITLE_HEIGHT + 130, 80.0, 28.0)];
    [button setBackgroundImage:[UIImage imageNamed:@"btn_shoufeidefault1.png"] forState:UIControlStateNormal];
    [button setBackgroundImage:[UIImage imageNamed:@"btn_shoufeiselected2.png"] forState:UIControlStateSelected];
    [button setTitle:@"完成交易" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.view addSubview:button];
    
    
    UILabel * orderLbl5 = [[UILabel alloc] initWithFrame:CGRectMake(0,DEVICE_HEIGHT - BOTTOM_HEIGHT -200 , DEVICE_WIDTH, 20)];
    orderLbl5.backgroundColor = [UIColor whiteColor];
    orderLbl5.font = [UIFont boldSystemFontOfSize:14.0];
    orderLbl5.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    orderLbl5.text = [NSString stringWithFormat:@"订单追踪"];
    [self.view addSubview:orderLbl5];
    
    UILabel * orderLbl6 = [[UILabel alloc] initWithFrame:CGRectMake(0,DEVICE_HEIGHT - BOTTOM_HEIGHT -180 , DEVICE_WIDTH, 20)];
    orderLbl6.backgroundColor = [UIColor whiteColor];
    orderLbl6.font = [UIFont boldSystemFontOfSize:14.0];
    orderLbl6.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    orderLbl6.text = [NSString stringWithFormat:@"2014-03-15 14:44      预约谢经理。"];
    [self.view addSubview:orderLbl6];

    
    
    
    

    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)clickOnLeftBut{
    [self.navigationController popViewControllerAnimated:TRUE];
}


/*
#pragma mark - Navigation
 

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
