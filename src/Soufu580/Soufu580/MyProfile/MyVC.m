//
//  MyVC.m
//  Soufu580
//
//  Created by  apple on 14-4-18.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "MyVC.h"
#import "UIImage+Resize.h"
#import "InvestProduct.h"
#import "ProductTableCell.h"
#import "InvestDetail.h"
#import "defines.h"

@interface MyVC ()<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray* vcArr;
    
    
    
}
@end

@implementation MyVC

- (id)init {
    self = [super init];
    if (self) {
        self.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我的投资" image:nil tag:0];
        
        [[self tabBarItem] setSelectedImage:[[UIImage imageNamed:@"jiezhangButSel"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        [[self tabBarItem] setImage:[[UIImage imageNamed:@"jiezhangBut"] resizedImageToFitInSize:CGSizeMake(30, 30) scaleIfSmaller:TRUE]];
        
        
        [[self tabBarItem] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                   [UIColor whiteColor], NSForegroundColorAttributeName,
                                                   [UIFont systemFontOfSize:12.0], NSFontAttributeName,
                                                   nil] forState:UIControlStateNormal];
        
        
        vcArr = [[NSMutableArray alloc]init];
        for(int i =0;i<9;i++){
            InvestProduct* prd = [[InvestProduct alloc]init];
            prd.orderId=[NSString stringWithFormat:@"ST20129456%d", i];
            prd.agent = @"高经理";
            prd.date=@"2013-11-1";
            prd.prdName=[NSString stringWithFormat:@"产品%d号信托",i];
            prd.amount=@"300万";
            prd.status=@"交易已完成";
            prd.phone = @"13098645710";
            [vcArr addObject:prd];
        }
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self addTopViewWithTitle:@"我的投资" leftButtonTitle:nil rightButtonTitle:nil];
    
   UITableView* vcTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, TITLE_HEIGHT,DEVICE_WIDTH, INVALID_HEIGHT-50 ) style:UITableViewStylePlain];
    vcTable.delegate = self;
    vcTable.dataSource = self;
    vcTable.backgroundColor = [UIColor clearColor];
    vcTable.bounces = NO;
    [self.view addSubview:vcTable];
    
    UILabel * stsLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, DEVICE_HEIGHT - BOTTOM_HEIGHT -50 , DEVICE_WIDTH, 50)];
    stsLbl.backgroundColor = [UIColor whiteColor];
    stsLbl.font = [UIFont boldSystemFontOfSize:14.0];
    stsLbl.textAlignment  = NSTextAlignmentCenter;
    stsLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    
    stsLbl.text = @"只显示最近6个月内的，查看更多请看网页版";
    [self.view addSubview:stsLbl];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [vcArr count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    InvestProduct * prd = [vcArr objectAtIndex:indexPath.row];
    ProductTableCell * cell = [tableView dequeueReusableCellWithIdentifier:kPrdCellIdentifier];
    
    if (cell==nil) {
        cell = [[ProductTableCell alloc] initWithProduct:prd];
        cell.userInteractionEnabled = YES;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
     InvestProduct * prd = [vcArr objectAtIndex:indexPath.row];
   InvestDetail* detail = [[InvestDetail alloc] initWithInvestProduct:prd];
    [self.navigationController pushViewController:detail animated:TRUE];
   
}

@end
