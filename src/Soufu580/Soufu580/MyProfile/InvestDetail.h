//
//  InvestDetail.h
//  Soufu580
//
//  Created by vidy on 14-4-23.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "BaseVC.h"
#import "InvestProduct.h"

@interface InvestDetail : BaseVC
-(id)initWithInvestProduct:(InvestProduct *)prd;
@end
