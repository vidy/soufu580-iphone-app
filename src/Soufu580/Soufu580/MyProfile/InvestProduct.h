//
//  InvestProduct.h
//  Soufu580
//
//  Created by vidy on 14-4-20.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InvestProduct : NSObject{
    NSString* _orderId; //订单ID
    NSString* _agent; //经理
    NSString* _date;
    NSString* _prdName;
    NSString* _amount;
    NSString* _status;
    NSString* _phone;
}
@property (nonatomic, retain) NSString * orderId;
@property (nonatomic, retain) NSString * agent;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * prdName;
@property (nonatomic, retain) NSString * amount;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * phone;



@end
