//
//  InvestProduct.m
//  Soufu580
//
//  Created by vidy on 14-4-20.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "InvestProduct.h"

@implementation InvestProduct
@synthesize orderId = _orderId;
@synthesize agent = _agent;
@synthesize date = _date;
@synthesize prdName = _prdName;
@synthesize amount = _amount;
@synthesize status = _status;
@synthesize phone = _phone;

@end
