//
//  ProductTableCell.h
//  Soufu580
//
//  Created by vidy on 14-4-20.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InvestProduct.h"
extern NSString * kPrdCellIdentifier;

@interface ProductTableCell : UITableViewCell
-(id)initWithProduct:(InvestProduct*) prd;
@end
