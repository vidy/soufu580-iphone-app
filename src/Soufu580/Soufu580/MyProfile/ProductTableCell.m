//
//  ProductTableCell.m
//  Soufu580
//
//  Created by vidy on 14-4-20.
//  Copyright (c) 2014年 Soufu580. All rights reserved.
//

#import "ProductTableCell.h"
#import "defines.h"

NSString * kPrdCellIdentifier = @"StrategyAbsCell";
@interface ProductTableCell () {
    InvestProduct * _prd;
}

@property (nonatomic, retain) InvestProduct * prd;
- (void)flushCellView;

@end


@implementation ProductTableCell

-(id)initWithProduct:(InvestProduct*) prd{
    
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kPrdCellIdentifier];
    if (self) {
        // Initialization code
        self.prd = prd;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    
    [self flushCellView];
    return self;
}
-(void) flushCellView{
    for (UIView * subView in self.subviews) {
        if (subView.tag>100) {
            [subView removeFromSuperview];
        }
    }
    
    UILabel * orderLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, DEVICE_WIDTH, 20)];
    orderLbl.backgroundColor = [UIColor clearColor];
    orderLbl.font = [UIFont boldSystemFontOfSize:14.0];
    orderLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    orderLbl.text = [NSString stringWithFormat:@"订单号: %@    %@  %@",self.prd.orderId,self.prd.agent,self.prd.date];
    [self addSubview:orderLbl];
    
    UILabel * prdLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, 135, 20)];
    prdLbl.backgroundColor = [UIColor clearColor];
    prdLbl.font = [UIFont boldSystemFontOfSize:14.0];
    prdLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];

    prdLbl.text = self.prd.prdName;
    [self addSubview:prdLbl];
    
    UILabel * amtLbl = [[UILabel alloc] initWithFrame:CGRectMake(250, 25, DEVICE_WIDTH - 250, 20)];
    amtLbl.backgroundColor = [UIColor clearColor];
    amtLbl.font = [UIFont boldSystemFontOfSize:14.0];
    amtLbl.textColor = [UIColor redColor];
    
    amtLbl.text = self.prd.amount;
    [self addSubview:amtLbl];
    
    UILabel * stsLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 50, 135, 20)];
    stsLbl.backgroundColor = [UIColor clearColor];
    stsLbl.font = [UIFont boldSystemFontOfSize:14.0];
    stsLbl.textColor = [UIColor colorWithRed:(86.0/255.0f) green:(86.0/255.0f) blue:(86.0/255.0f) alpha:1.0];
    
    stsLbl.text = self.prd.status;
    [self addSubview:stsLbl];
    
    
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

   
    // Configure the view for the selected state
}

@end
